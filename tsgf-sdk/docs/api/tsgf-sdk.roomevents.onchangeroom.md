<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [tsgf-sdk](./tsgf-sdk.md) &gt; [RoomEvents](./tsgf-sdk.roomevents.md) &gt; [onChangeRoom](./tsgf-sdk.roomevents.onchangeroom.md)

## RoomEvents.onChangeRoom() method

【在房间中才能收到】房间信息有修改

<b>Signature:</b>

```typescript
onChangeRoom(fn: (roomInfo: IRoomInfo) => void): void;
```

## Parameters

|  Parameter | Type | Description |
|  --- | --- | --- |
|  fn | (roomInfo: [IRoomInfo](./tsgf-sdk.iroominfo.md)<!-- -->) =&gt; void |  |

<b>Returns:</b>

void

