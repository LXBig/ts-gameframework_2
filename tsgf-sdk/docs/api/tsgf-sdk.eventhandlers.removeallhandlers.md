<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [tsgf-sdk](./tsgf-sdk.md) &gt; [EventHandlers](./tsgf-sdk.eventhandlers.md) &gt; [removeAllHandlers](./tsgf-sdk.eventhandlers.removeallhandlers.md)

## EventHandlers.removeAllHandlers() method

Removes all handlers

<b>Signature:</b>

```typescript
removeAllHandlers(): void;
```
<b>Returns:</b>

void

