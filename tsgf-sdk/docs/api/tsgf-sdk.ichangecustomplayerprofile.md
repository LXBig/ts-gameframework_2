<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [tsgf-sdk](./tsgf-sdk.md) &gt; [IChangeCustomPlayerProfile](./tsgf-sdk.ichangecustomplayerprofile.md)

## IChangeCustomPlayerProfile interface

玩家自定义信息的变更信息

<b>Signature:</b>

```typescript
export interface IChangeCustomPlayerProfile 
```

## Properties

|  Property | Type | Description |
|  --- | --- | --- |
|  [changePlayerId](./tsgf-sdk.ichangecustomplayerprofile.changeplayerid.md) | string |  |
|  [customPlayerProfile](./tsgf-sdk.ichangecustomplayerprofile.customplayerprofile.md) | string |  |
|  [oldCustomPlayerProfile](./tsgf-sdk.ichangecustomplayerprofile.oldcustomplayerprofile.md) | string |  |
|  [roomInfo](./tsgf-sdk.ichangecustomplayerprofile.roominfo.md) | [IRoomInfo](./tsgf-sdk.iroominfo.md) |  |

