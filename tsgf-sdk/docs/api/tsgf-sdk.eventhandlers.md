<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [tsgf-sdk](./tsgf-sdk.md) &gt; [EventHandlers](./tsgf-sdk.eventhandlers.md)

## EventHandlers class

单事件的多处理器订阅和触发

<b>Signature:</b>

```typescript
export declare class EventHandlers<FunctionType extends Function> 
```

## Constructors

|  Constructor | Modifiers | Description |
|  --- | --- | --- |
|  [(constructor)()](./tsgf-sdk.eventhandlers._constructor_.md) |  | 构造 |

## Methods

|  Method | Modifiers | Description |
|  --- | --- | --- |
|  [addHandler(handler, target)](./tsgf-sdk.eventhandlers.addhandler.md) |  | 添加处理器 |
|  [count()](./tsgf-sdk.eventhandlers.count.md) |  | Counts event handlers |
|  [emit(args)](./tsgf-sdk.eventhandlers.emit.md) |  | 触发所有处理器, 有处理器则返回true |
|  [removeAllHandlers()](./tsgf-sdk.eventhandlers.removeallhandlers.md) |  | Removes all handlers |
|  [removeHandler(handler)](./tsgf-sdk.eventhandlers.removehandler.md) |  | 移出处理器 |

