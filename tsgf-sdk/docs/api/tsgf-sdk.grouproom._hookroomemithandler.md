<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [tsgf-sdk](./tsgf-sdk.md) &gt; [GroupRoom](./tsgf-sdk.grouproom.md) &gt; [\_hookRoomEmitHandler](./tsgf-sdk.grouproom._hookroomemithandler.md)

## GroupRoom.\_hookRoomEmitHandler() method

<b>Signature:</b>

```typescript
protected _hookRoomEmitHandler(key: keyof RoomEvents, bindGroupHandler: Function): void;
```

## Parameters

|  Parameter | Type | Description |
|  --- | --- | --- |
|  key | keyof [RoomEvents](./tsgf-sdk.roomevents.md) |  |
|  bindGroupHandler | Function |  |

<b>Returns:</b>

void

