<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [tsgf-sdk](./tsgf-sdk.md) &gt; [RoomEvents](./tsgf-sdk.roomevents.md) &gt; [offLeaveRoom](./tsgf-sdk.roomevents.offleaveroom.md)

## RoomEvents.offLeaveRoom() method

<b>Signature:</b>

```typescript
offLeaveRoom(fn: (roomInfo: IRoomInfo) => void): void;
```

## Parameters

|  Parameter | Type | Description |
|  --- | --- | --- |
|  fn | (roomInfo: [IRoomInfo](./tsgf-sdk.iroominfo.md)<!-- -->) =&gt; void |  |

<b>Returns:</b>

void

