<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [tsgf-sdk](./tsgf-sdk.md) &gt; [GameClient](./tsgf-sdk.gameclient.md) &gt; [dismissRoom](./tsgf-sdk.gameclient.dismissroom.md)

## GameClient.dismissRoom() method

【仅房主】解散当前房间

<b>Signature:</b>

```typescript
dismissRoom(): Promise<IResult<IRoomInfo>>;
```
<b>Returns:</b>

Promise&lt;[IResult](./tsgf-sdk.iresult.md)<!-- -->&lt;[IRoomInfo](./tsgf-sdk.iroominfo.md)<!-- -->&gt;&gt;


