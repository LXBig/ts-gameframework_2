<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [tsgf-sdk](./tsgf-sdk.md) &gt; [Result](./tsgf-sdk.result.md) &gt; [buildSucc](./tsgf-sdk.result.buildsucc.md)

## Result.buildSucc() method

构建一个成功的结果对象

<b>Signature:</b>

```typescript
static buildSucc<T>(data: T): IResult<T>;
```

## Parameters

|  Parameter | Type | Description |
|  --- | --- | --- |
|  data | T |  |

<b>Returns:</b>

[IResult](./tsgf-sdk.iresult.md)<!-- -->&lt;T&gt;


