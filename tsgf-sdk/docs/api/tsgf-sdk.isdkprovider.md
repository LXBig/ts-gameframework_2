<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [tsgf-sdk](./tsgf-sdk.md) &gt; [ISDKProvider](./tsgf-sdk.isdkprovider.md)

## ISDKProvider interface

全局供应商接口定义

<b>Signature:</b>

```typescript
export interface ISDKProvider 
```

## Properties

|  Property | Type | Description |
|  --- | --- | --- |
|  [env](./tsgf-sdk.isdkprovider.env.md) | [IEnvProvider](./tsgf-sdk.ienvprovider.md) \| null | 环境实现供应商 |

