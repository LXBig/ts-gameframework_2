<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [tsgf-sdk](./tsgf-sdk.md) &gt; [Room](./tsgf-sdk.room.md) &gt; [sendFrame](./tsgf-sdk.room.sendframe.md)

## Room.sendFrame() method

发送玩家输入帧(加入到下一帧的操作列表)

<b>Signature:</b>

```typescript
sendFrame(inpOperates: IPlayerInputOperate[]): Promise<IResult<null>>;
```

## Parameters

|  Parameter | Type | Description |
|  --- | --- | --- |
|  inpOperates | [IPlayerInputOperate](./tsgf-sdk.iplayerinputoperate.md)<!-- -->\[\] |  |

<b>Returns:</b>

Promise&lt;[IResult](./tsgf-sdk.iresult.md)<!-- -->&lt;null&gt;&gt;

