<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [tsgf-sdk](./tsgf-sdk.md) &gt; [IChangeCustomPlayerProfile](./tsgf-sdk.ichangecustomplayerprofile.md) &gt; [changePlayerId](./tsgf-sdk.ichangecustomplayerprofile.changeplayerid.md)

## IChangeCustomPlayerProfile.changePlayerId property

<b>Signature:</b>

```typescript
changePlayerId: string;
```
