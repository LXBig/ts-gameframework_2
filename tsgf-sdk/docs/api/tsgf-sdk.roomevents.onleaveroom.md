<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [tsgf-sdk](./tsgf-sdk.md) &gt; [RoomEvents](./tsgf-sdk.roomevents.md) &gt; [onLeaveRoom](./tsgf-sdk.roomevents.onleaveroom.md)

## RoomEvents.onLeaveRoom() method

当前玩家不管什么原因离开了房间(主动离开,主动解散,房间被解散等等),都会触发

<b>Signature:</b>

```typescript
onLeaveRoom(fn: (roomInfo: IRoomInfo) => void): void;
```

## Parameters

|  Parameter | Type | Description |
|  --- | --- | --- |
|  fn | (roomInfo: [IRoomInfo](./tsgf-sdk.iroominfo.md)<!-- -->) =&gt; void |  |

<b>Returns:</b>

void

