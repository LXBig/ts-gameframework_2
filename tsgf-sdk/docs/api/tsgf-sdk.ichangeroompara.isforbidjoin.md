<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [tsgf-sdk](./tsgf-sdk.md) &gt; [IChangeRoomPara](./tsgf-sdk.ichangeroompara.md) &gt; [isForbidJoin](./tsgf-sdk.ichangeroompara.isforbidjoin.md)

## IChangeRoomPara.isForbidJoin property

\[不修改请不要赋值\] 是否不允许加人(任何方式都无法加入)

<b>Signature:</b>

```typescript
isForbidJoin?: boolean;
```
