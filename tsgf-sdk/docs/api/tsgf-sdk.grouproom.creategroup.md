<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [tsgf-sdk](./tsgf-sdk.md) &gt; [GroupRoom](./tsgf-sdk.grouproom.md) &gt; [createGroup](./tsgf-sdk.grouproom.creategroup.md)

## GroupRoom.createGroup() method

创建一个组队房间并进入, 之前有在其他房间将自动退出, 成功则 this.currGroupRoom 有值

<b>Signature:</b>

```typescript
createGroup(playerPara: IPlayerInfoPara): Promise<IResult<string>>;
```

## Parameters

|  Parameter | Type | Description |
|  --- | --- | --- |
|  playerPara | [IPlayerInfoPara](./tsgf-sdk.iplayerinfopara.md) |  |

<b>Returns:</b>

Promise&lt;[IResult](./tsgf-sdk.iresult.md)<!-- -->&lt;string&gt;&gt;

groupRoomId

