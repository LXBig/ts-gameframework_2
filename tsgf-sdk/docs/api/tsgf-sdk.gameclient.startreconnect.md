<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [tsgf-sdk](./tsgf-sdk.md) &gt; [GameClient](./tsgf-sdk.gameclient.md) &gt; [startReconnect](./tsgf-sdk.gameclient.startreconnect.md)

## GameClient.startReconnect() method

Starts reconnect

<b>Signature:</b>

```typescript
protected startReconnect(currTryCount?: number, failReTry?: boolean): Promise<boolean>;
```

## Parameters

|  Parameter | Type | Description |
|  --- | --- | --- |
|  currTryCount | number | <i>(Optional)</i> 当前重试次数 |
|  failReTry | boolean | <i>(Optional)</i> 本次失败后是否继续重试 |

<b>Returns:</b>

Promise&lt;boolean&gt;

reconnect

