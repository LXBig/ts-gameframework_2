<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [tsgf-sdk](./tsgf-sdk.md) &gt; [IMatchParamsFromPlayer](./tsgf-sdk.imatchparamsfromplayer.md)

## IMatchParamsFromPlayer interface

单独玩家发起的匹配参数

<b>Signature:</b>

```typescript
export interface IMatchParamsFromPlayer extends IMatchParamsBase 
```
<b>Extends:</b> [IMatchParamsBase](./tsgf-sdk.imatchparamsbase.md)

## Properties

|  Property | Type | Description |
|  --- | --- | --- |
|  [matchFromInfo](./tsgf-sdk.imatchparamsfromplayer.matchfrominfo.md) | [IMatchFromPlayer](./tsgf-sdk.imatchfromplayer.md) | 匹配发起的玩家信息, 注意,这些玩家不会收到服务器通知 |
|  [matchFromType](./tsgf-sdk.imatchparamsfromplayer.matchfromtype.md) | [EMatchFromType.Player](./tsgf-sdk.ematchfromtype.md) | 发起类型是玩家 |

