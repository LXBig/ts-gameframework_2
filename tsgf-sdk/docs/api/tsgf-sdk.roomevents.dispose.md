<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [tsgf-sdk](./tsgf-sdk.md) &gt; [RoomEvents](./tsgf-sdk.roomevents.md) &gt; [dispose](./tsgf-sdk.roomevents.dispose.md)

## RoomEvents.dispose() method

<b>Signature:</b>

```typescript
dispose(): Promise<void>;
```
<b>Returns:</b>

Promise&lt;void&gt;

