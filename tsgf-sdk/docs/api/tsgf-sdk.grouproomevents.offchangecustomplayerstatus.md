<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [tsgf-sdk](./tsgf-sdk.md) &gt; [GroupRoomEvents](./tsgf-sdk.grouproomevents.md) &gt; [offChangeCustomPlayerStatus](./tsgf-sdk.grouproomevents.offchangecustomplayerstatus.md)

## GroupRoomEvents.offChangeCustomPlayerStatus() method

<b>Signature:</b>

```typescript
offChangeCustomPlayerStatus(fn: (changeInfo: IChangeCustomPlayerStatus) => void): void;
```

## Parameters

|  Parameter | Type | Description |
|  --- | --- | --- |
|  fn | (changeInfo: [IChangeCustomPlayerStatus](./tsgf-sdk.ichangecustomplayerstatus.md)<!-- -->) =&gt; void |  |

<b>Returns:</b>

void

