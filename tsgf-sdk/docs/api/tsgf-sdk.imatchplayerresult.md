<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [tsgf-sdk](./tsgf-sdk.md) &gt; [IMatchPlayerResult](./tsgf-sdk.imatchplayerresult.md)

## IMatchPlayerResult interface

匹配请求的单个玩家结果

<b>Signature:</b>

```typescript
export interface IMatchPlayerResult 
```

## Properties

|  Property | Type | Description |
|  --- | --- | --- |
|  [playerId](./tsgf-sdk.imatchplayerresult.playerid.md) | string | 玩家id |
|  [teamId?](./tsgf-sdk.imatchplayerresult.teamid.md) | string | <i>(Optional)</i> 应该加入的队伍id |

