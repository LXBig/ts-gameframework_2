import { IPlayerInfo } from "../player/IPlayerInfo";

/**创建房间的方式*/
export enum ERoomCreateType {
    /**调用创建房间方法创建的*/
    COMMON_CREATE = 0,
    /**由匹配创建的*/
    MATCH_CREATE = 1,
}

/**帧同步状态*/
export enum EFrameSyncState {
    /**未开始帧同步*/
    STOP = 0,
    /**已开始帧同步*/
    START = 1,
}

/**房间信息*/
export interface IRoomInfo {
    /**房间ID*/
    roomId: string;
    /**房间名称*/
    roomName: string;
    /**房主玩家ID，创建后，只有房主玩家的客户端才可以调用相关的管理操作*/
    ownerPlayerId: string;
    /**是否私有房间，即不参与匹配, 但可以通过房间ID加入*/
    isPrivate: boolean;
    /**是否不允许加人(任何方式都无法加入)*/
    isForbidJoin: boolean;
    /**如果参与匹配,则使用的匹配器标识*/
    matcherKey?: string;
    /**创建房间的方式*/
    createType: ERoomCreateType;
    /**进入房间的最大玩家数量*/
    maxPlayers: number;
    /**房间类型自定义字符串*/
    roomType?: string;
    /**自定义房间属性字符串*/
    customProperties?: string;

    /**如果当前房间在匹配 (房间全玩家匹配),则有匹配请求id*/
    allPlayerMatchReqId?: string;

    /**玩家列表*/
    playerList: IPlayerInfo[];
    /**队伍列表,如果创建房间时有传入队伍参数,则会有内容,否则为[]*/
    teamList: ITeamInfo[];
    /**[固定数量的队伍] 直接用数量自动生成所有固定队伍配置, 房间ID将从 '1' 开始到 fixedTeamCount, 
     * 还需要传 fixedTeamMinPlayers 和 fixedTeamMaxPlayers, 或者使用 fixedTeamInfoList 完全自定义 */
    fixedTeamCount?: number;
    /**[自由数量的队伍] 指定每支队伍的最少玩家数, 同时还需要指定 freeTeamMaxPlayers*/
    freeTeamMinPlayers?: number;
    /**[自由数量的队伍] 指定每支队伍的最大玩家数, 同时还需要指定 freeTeamMinPlayers*/
    freeTeamMaxPlayers?: number;

    /**同步帧率*/
    frameRate: number;
    /**帧同步状态*/
    frameSyncState: EFrameSyncState;
    /**创建房间时间戳（单位毫秒， new Date(createTime) 可获得时间对象）*/
    createTime: number;
    /**开始游戏时间戳（单位毫秒， new Date(createTime) 可获得时间对象）,0表示未开始*/
    startGameTime: number;
}

/**队伍的配置参数*/
export interface ITeamParams {

    /**[固定数量的队伍] 直接用数量自动生成所有固定队伍配置, 房间ID将从 '1' 开始到 fixedTeamCount, 
     * 还需要传 fixedTeamMinPlayers 和 fixedTeamMaxPlayers, 或者使用 fixedTeamInfoList 完全自定义 */
    fixedTeamCount?: number;
    /**[固定数量的队伍] 每支队伍最少玩家数(包含), 没传默认为1*/
    fixedTeamMinPlayers?: number;
    /**[固定数量的队伍] 每支队伍最大玩家数(包含), 没传默认为9*/
    fixedTeamMaxPlayers?: number;
    /**[固定数量的队伍] 使用传入的队伍id等信息来定义所有队伍, 并忽略 fixedTeam* 的其他参数*/
    fixedTeamInfoList?: ITeamInfo[];

    /**[自由数量的队伍] 指定每支队伍的最少玩家数, 同时还需要指定 freeTeamMaxPlayers*/
    freeTeamMinPlayers?: number;
    /**[自由数量的队伍] 指定每支队伍的最大玩家数, 同时还需要指定 freeTeamMinPlayers*/
    freeTeamMaxPlayers?: number;

}

/**创建房间的参数*/
export interface ICreateRoomPara extends ITeamParams {
    /**房间名字，查询房间和加入房间时会获取到*/
    roomName: string;
    /**房主玩家ID，创建后，只有房主玩家的客户端才可以调用相关的管理操作, 如果不想任何人操作,可以直接设置为'', 所有人都离开房间后自动解散*/
    ownerPlayerId: string;
    /**进入房间的最大玩家数量*/
    maxPlayers: number;
    /**是否私有房间，即不参与匹配, 但可以通过房间ID加入*/
    isPrivate: boolean;
    /**如果参与匹配,则使用的匹配器标识*/
    matcherKey?: string;
    /**自定义房间属性字符串*/
    customProperties?: string;
    /**房间类型自定义字符串*/
    roomType?: string;
}
/**创建房间的响应*/
export interface ICreateRoomRsp {
    /**游戏服务器的连接地址*/
    gameServerUrl: string;
    /**房间信息*/
    roomInfo: IRoomInfo;
}

/**房间在服务器上的注册信息*/
export interface IRoomRegInfo {
    /**游戏服务器的连接地址*/
    gameServerUrl: string;
}


/**修改房间信息的参数*/
export interface IChangeRoomPara {
    /**[不修改请不要赋值] 房间名称*/
    roomName?: string;
    /**[不修改请不要赋值] 房主玩家ID，创建后，只有房主玩家的客户端才可以调用相关的管理操作*/
    ownerPlayerId?: string;
    /**[不修改请不要赋值] 是否私有房间，即不参与匹配, 但可以通过房间ID加入*/
    isPrivate?: boolean;
    /**[不修改请不要赋值] 是否不允许加人(任何方式都无法加入)*/
    isForbidJoin?: boolean;
    /**[不修改请不要赋值] 自定义房间属性字符串*/
    customProperties?: string;
}


export interface ITeamInfo {
    /**队伍 ID, 房间内唯一*/
    id: string;
    /**队伍名称*/
    name: string;
    /**队伍最小人数*/
    minPlayers: number;
    /**队伍最大人数*/
    maxPlayers: number;
}


/**队伍的玩家id列表*/
export interface ITeamPlayerIds {
    /**所属队伍id,如果没有队伍则为 undefined*/
    teamId?: string;
    /**玩家id列表*/
    playerIds: string[];
}