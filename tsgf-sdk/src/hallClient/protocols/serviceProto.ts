import { ServiceProto } from 'tsrpc-proto';
import { ReqAuthorize, ResAuthorize } from './PtlAuthorize';
import { ReqCancelMatch, ResCancelMatch } from './PtlCancelMatch';
import { ReqCreateRoom, ResCreateRoom } from './PtlCreateRoom';
import { ReqGetRoomRegInfo, ResGetRoomRegInfo } from './PtlGetRoomRegInfo';
import { ReqQueryMatch, ResQueryMatch } from './PtlQueryMatch';
import { ReqRequestMatch, ResRequestMatch } from './PtlRequestMatch';

export interface ServiceType {
    api: {
        "Authorize": {
            req: ReqAuthorize,
            res: ResAuthorize
        },
        "CancelMatch": {
            req: ReqCancelMatch,
            res: ResCancelMatch
        },
        "CreateRoom": {
            req: ReqCreateRoom,
            res: ResCreateRoom
        },
        "GetRoomRegInfo": {
            req: ReqGetRoomRegInfo,
            res: ResGetRoomRegInfo
        },
        "QueryMatch": {
            req: ReqQueryMatch,
            res: ResQueryMatch
        },
        "RequestMatch": {
            req: ReqRequestMatch,
            res: ResRequestMatch
        }
    },
    msg: {

    }
}

export const serviceProto: ServiceProto<ServiceType> = {
    "version": 16,
    "services": [
        {
            "id": 0,
            "name": "Authorize",
            "type": "api",
            "conf": {
                "skipAuth": true,
                "cryptoMode": "AppReqDes"
            }
        },
        {
            "id": 1,
            "name": "CancelMatch",
            "type": "api",
            "conf": {
                "cryptoMode": "None"
            }
        },
        {
            "id": 2,
            "name": "CreateRoom",
            "type": "api",
            "conf": {
                "cryptoMode": "None"
            }
        },
        {
            "id": 3,
            "name": "GetRoomRegInfo",
            "type": "api",
            "conf": {
                "cryptoMode": "None"
            }
        },
        {
            "id": 4,
            "name": "QueryMatch",
            "type": "api",
            "conf": {
                "cryptoMode": "None"
            }
        },
        {
            "id": 5,
            "name": "RequestMatch",
            "type": "api",
            "conf": {
                "cryptoMode": "None"
            }
        }
    ],
    "types": {
        "PtlAuthorize/ReqAuthorize": {
            "type": "Interface",
            "extends": [
                {
                    "id": 0,
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/apiCrypto/Models/IAppEncryptRequest"
                    }
                }
            ]
        },
        "../../tsgf/apiCrypto/Models/IAppEncryptRequest": {
            "type": "Interface",
            "extends": [
                {
                    "id": 0,
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/apiCrypto/Models/IBaseEncryptRequest"
                    }
                }
            ],
            "properties": [
                {
                    "id": 0,
                    "name": "appId",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "../../tsgf/apiCrypto/Models/IBaseEncryptRequest": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "ciphertext",
                    "type": {
                        "type": "String"
                    },
                    "optional": true
                },
                {
                    "id": 1,
                    "name": "data",
                    "type": {
                        "type": "Any"
                    },
                    "optional": true
                }
            ]
        },
        "PtlAuthorize/ResAuthorize": {
            "type": "Interface",
            "extends": [
                {
                    "id": 0,
                    "type": {
                        "type": "Reference",
                        "target": "base/BaseResponse"
                    }
                }
            ],
            "properties": [
                {
                    "id": 0,
                    "name": "playerId",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "playerToken",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "base/BaseResponse": {
            "type": "Interface"
        },
        "PtlCancelMatch/ReqCancelMatch": {
            "type": "Interface",
            "extends": [
                {
                    "id": 0,
                    "type": {
                        "type": "Reference",
                        "target": "base/BaseRequest"
                    }
                }
            ],
            "properties": [
                {
                    "id": 0,
                    "name": "matchReqId",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "base/BaseRequest": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "playerToken",
                    "type": {
                        "type": "String"
                    },
                    "optional": true
                }
            ]
        },
        "PtlCancelMatch/ResCancelMatch": {
            "type": "Interface",
            "extends": [
                {
                    "id": 0,
                    "type": {
                        "type": "Reference",
                        "target": "base/BaseResponse"
                    }
                }
            ]
        },
        "PtlCreateRoom/ReqCreateRoom": {
            "type": "Interface",
            "extends": [
                {
                    "id": 0,
                    "type": {
                        "type": "Reference",
                        "target": "base/BaseRequest"
                    }
                },
                {
                    "id": 1,
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/room/IRoomInfo/ICreateRoomPara"
                    }
                }
            ]
        },
        "../../tsgf/room/IRoomInfo/ICreateRoomPara": {
            "type": "Interface",
            "extends": [
                {
                    "id": 0,
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/room/IRoomInfo/ITeamParams"
                    }
                }
            ],
            "properties": [
                {
                    "id": 0,
                    "name": "roomName",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "ownerPlayerId",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 2,
                    "name": "maxPlayers",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 3,
                    "name": "isPrivate",
                    "type": {
                        "type": "Boolean"
                    }
                },
                {
                    "id": 6,
                    "name": "matcherKey",
                    "type": {
                        "type": "String"
                    },
                    "optional": true
                },
                {
                    "id": 4,
                    "name": "customProperties",
                    "type": {
                        "type": "String"
                    },
                    "optional": true
                },
                {
                    "id": 5,
                    "name": "roomType",
                    "type": {
                        "type": "String"
                    },
                    "optional": true
                }
            ]
        },
        "../../tsgf/room/IRoomInfo/ITeamParams": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "fixedTeamCount",
                    "type": {
                        "type": "Number"
                    },
                    "optional": true
                },
                {
                    "id": 1,
                    "name": "fixedTeamMinPlayers",
                    "type": {
                        "type": "Number"
                    },
                    "optional": true
                },
                {
                    "id": 2,
                    "name": "fixedTeamMaxPlayers",
                    "type": {
                        "type": "Number"
                    },
                    "optional": true
                },
                {
                    "id": 3,
                    "name": "fixedTeamInfoList",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "../../tsgf/room/IRoomInfo/ITeamInfo"
                        }
                    },
                    "optional": true
                },
                {
                    "id": 4,
                    "name": "freeTeamMinPlayers",
                    "type": {
                        "type": "Number"
                    },
                    "optional": true
                },
                {
                    "id": 5,
                    "name": "freeTeamMaxPlayers",
                    "type": {
                        "type": "Number"
                    },
                    "optional": true
                }
            ]
        },
        "../../tsgf/room/IRoomInfo/ITeamInfo": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "id",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "name",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 2,
                    "name": "minPlayers",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 3,
                    "name": "maxPlayers",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "PtlCreateRoom/ResCreateRoom": {
            "type": "Interface",
            "extends": [
                {
                    "id": 0,
                    "type": {
                        "type": "Reference",
                        "target": "base/BaseResponse"
                    }
                },
                {
                    "id": 1,
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/room/IRoomInfo/ICreateRoomRsp"
                    }
                }
            ]
        },
        "../../tsgf/room/IRoomInfo/ICreateRoomRsp": {
            "type": "Interface",
            "properties": [
                {
                    "id": 2,
                    "name": "gameServerUrl",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "roomInfo",
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/room/IRoomInfo/IRoomInfo"
                    }
                }
            ]
        },
        "../../tsgf/room/IRoomInfo/IRoomInfo": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "roomId",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "roomName",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 2,
                    "name": "ownerPlayerId",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 3,
                    "name": "isPrivate",
                    "type": {
                        "type": "Boolean"
                    }
                },
                {
                    "id": 4,
                    "name": "isForbidJoin",
                    "type": {
                        "type": "Boolean"
                    }
                },
                {
                    "id": 14,
                    "name": "matcherKey",
                    "type": {
                        "type": "String"
                    },
                    "optional": true
                },
                {
                    "id": 5,
                    "name": "createType",
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/room/IRoomInfo/ERoomCreateType"
                    }
                },
                {
                    "id": 6,
                    "name": "maxPlayers",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 7,
                    "name": "roomType",
                    "type": {
                        "type": "String"
                    },
                    "optional": true
                },
                {
                    "id": 8,
                    "name": "customProperties",
                    "type": {
                        "type": "String"
                    },
                    "optional": true
                },
                {
                    "id": 19,
                    "name": "allPlayerMatchReqId",
                    "type": {
                        "type": "String"
                    },
                    "optional": true
                },
                {
                    "id": 9,
                    "name": "playerList",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "../../tsgf/player/IPlayerInfo/IPlayerInfo"
                        }
                    }
                },
                {
                    "id": 15,
                    "name": "teamList",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "../../tsgf/room/IRoomInfo/ITeamInfo"
                        }
                    }
                },
                {
                    "id": 16,
                    "name": "fixedTeamCount",
                    "type": {
                        "type": "Number"
                    },
                    "optional": true
                },
                {
                    "id": 17,
                    "name": "freeTeamMinPlayers",
                    "type": {
                        "type": "Number"
                    },
                    "optional": true
                },
                {
                    "id": 18,
                    "name": "freeTeamMaxPlayers",
                    "type": {
                        "type": "Number"
                    },
                    "optional": true
                },
                {
                    "id": 10,
                    "name": "frameRate",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 11,
                    "name": "frameSyncState",
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/room/IRoomInfo/EFrameSyncState"
                    }
                },
                {
                    "id": 12,
                    "name": "createTime",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 13,
                    "name": "startGameTime",
                    "type": {
                        "type": "Number"
                    }
                }
            ]
        },
        "../../tsgf/room/IRoomInfo/ERoomCreateType": {
            "type": "Enum",
            "members": [
                {
                    "id": 0,
                    "value": 0
                },
                {
                    "id": 1,
                    "value": 1
                }
            ]
        },
        "../../tsgf/player/IPlayerInfo/IPlayerInfo": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "playerId",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "showName",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 2,
                    "name": "teamId",
                    "type": {
                        "type": "String"
                    },
                    "optional": true
                },
                {
                    "id": 3,
                    "name": "customPlayerStatus",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 9,
                    "name": "customPlayerProfile",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 8,
                    "name": "networkState",
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/player/IPlayerInfo/ENetworkState"
                    }
                },
                {
                    "id": 7,
                    "name": "isRobot",
                    "type": {
                        "type": "Boolean"
                    }
                }
            ]
        },
        "../../tsgf/player/IPlayerInfo/ENetworkState": {
            "type": "Enum",
            "members": [
                {
                    "id": 0,
                    "value": 0
                },
                {
                    "id": 1,
                    "value": 1
                }
            ]
        },
        "../../tsgf/room/IRoomInfo/EFrameSyncState": {
            "type": "Enum",
            "members": [
                {
                    "id": 0,
                    "value": 0
                },
                {
                    "id": 1,
                    "value": 1
                }
            ]
        },
        "PtlGetRoomRegInfo/ReqGetRoomRegInfo": {
            "type": "Interface",
            "extends": [
                {
                    "id": 0,
                    "type": {
                        "type": "Reference",
                        "target": "base/BaseRequest"
                    }
                }
            ],
            "properties": [
                {
                    "id": 0,
                    "name": "roomId",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "PtlGetRoomRegInfo/ResGetRoomRegInfo": {
            "type": "Interface",
            "extends": [
                {
                    "id": 0,
                    "type": {
                        "type": "Reference",
                        "target": "base/BaseResponse"
                    }
                }
            ],
            "properties": [
                {
                    "id": 0,
                    "name": "regInfo",
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/room/IRoomInfo/IRoomRegInfo"
                    }
                }
            ]
        },
        "../../tsgf/room/IRoomInfo/IRoomRegInfo": {
            "type": "Interface",
            "properties": [
                {
                    "id": 1,
                    "name": "gameServerUrl",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "PtlQueryMatch/ReqQueryMatch": {
            "type": "Interface",
            "extends": [
                {
                    "id": 0,
                    "type": {
                        "type": "Reference",
                        "target": "base/BaseRequest"
                    }
                }
            ],
            "properties": [
                {
                    "id": 0,
                    "name": "matchReqId",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        },
        "PtlQueryMatch/ResQueryMatch": {
            "type": "Interface",
            "extends": [
                {
                    "id": 0,
                    "type": {
                        "type": "Reference",
                        "target": "base/BaseResponse"
                    }
                }
            ],
            "properties": [
                {
                    "id": 0,
                    "name": "hasResult",
                    "type": {
                        "type": "Boolean"
                    }
                },
                {
                    "id": 3,
                    "name": "errMsg",
                    "type": {
                        "type": "String"
                    },
                    "optional": true
                },
                {
                    "id": 4,
                    "name": "errCode",
                    "type": {
                        "type": "Number"
                    },
                    "optional": true
                },
                {
                    "id": 2,
                    "name": "matchResult",
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/match/Models/IMatchResult"
                    },
                    "optional": true
                }
            ]
        },
        "../../tsgf/match/Models/IMatchResult": {
            "type": "Interface",
            "properties": [
                {
                    "id": 1,
                    "name": "gameServerUrl",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 0,
                    "name": "roomId",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 2,
                    "name": "matchPlayerResults",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "Reference",
                            "target": "../../tsgf/match/Models/IMatchPlayerResult"
                        }
                    }
                }
            ]
        },
        "../../tsgf/match/Models/IMatchPlayerResult": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "playerId",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 1,
                    "name": "teamId",
                    "type": {
                        "type": "String"
                    },
                    "optional": true
                }
            ]
        },
        "PtlRequestMatch/ReqRequestMatch": {
            "type": "Interface",
            "extends": [
                {
                    "id": 0,
                    "type": {
                        "type": "Reference",
                        "target": "base/BaseRequest"
                    }
                }
            ],
            "properties": [
                {
                    "id": 0,
                    "name": "matchParams",
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/match/Models/IMatchParamsFromPlayer"
                    }
                }
            ]
        },
        "../../tsgf/match/Models/IMatchParamsFromPlayer": {
            "type": "Interface",
            "extends": [
                {
                    "id": 0,
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/match/Models/IMatchParamsBase"
                    }
                }
            ],
            "properties": [
                {
                    "id": 0,
                    "name": "matchFromType",
                    "type": {
                        "type": "Literal",
                        "literal": "Player"
                    }
                },
                {
                    "id": 1,
                    "name": "matchFromInfo",
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/match/Models/IMatchFromPlayer"
                    }
                }
            ]
        },
        "../../tsgf/match/Models/IMatchParamsBase": {
            "type": "Interface",
            "properties": [
                {
                    "id": 6,
                    "name": "matchType",
                    "type": {
                        "type": "String"
                    },
                    "optional": true
                },
                {
                    "id": 0,
                    "name": "matcherKey",
                    "type": {
                        "type": "String"
                    }
                },
                {
                    "id": 4,
                    "name": "matcherParams",
                    "type": {
                        "type": "Any"
                    }
                },
                {
                    "id": 2,
                    "name": "matchTimeoutSec",
                    "type": {
                        "type": "Number"
                    },
                    "optional": true
                },
                {
                    "id": 3,
                    "name": "maxPlayers",
                    "type": {
                        "type": "Number"
                    }
                },
                {
                    "id": 5,
                    "name": "teamParams",
                    "type": {
                        "type": "Reference",
                        "target": "../../tsgf/room/IRoomInfo/ITeamParams"
                    },
                    "optional": true
                }
            ]
        },
        "../../tsgf/match/Models/EMatchFromType": {
            "type": "Enum",
            "members": [
                {
                    "id": 3,
                    "value": "Player"
                },
                {
                    "id": 4,
                    "value": "RoomJoinUs"
                },
                {
                    "id": 5,
                    "value": "RoomAllPlayers"
                }
            ]
        },
        "../../tsgf/match/Models/IMatchFromPlayer": {
            "type": "Interface",
            "properties": [
                {
                    "id": 0,
                    "name": "playerIds",
                    "type": {
                        "type": "Array",
                        "elementType": {
                            "type": "String"
                        }
                    }
                }
            ]
        },
        "PtlRequestMatch/ResRequestMatch": {
            "type": "Interface",
            "extends": [
                {
                    "id": 0,
                    "type": {
                        "type": "Reference",
                        "target": "base/BaseResponse"
                    }
                }
            ],
            "properties": [
                {
                    "id": 0,
                    "name": "matchReqId",
                    "type": {
                        "type": "String"
                    }
                }
            ]
        }
    }
};