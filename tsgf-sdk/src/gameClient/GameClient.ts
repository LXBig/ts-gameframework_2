

import { BaseWsClient, BaseWsClientOptions } from "tsrpc-base-client";
import { ServiceProto } from "tsrpc-proto";

import { serviceProto as gameServiceProto, ServiceType } from "./protocols/serviceProto";
import { AWsClient } from "../tsgf/AClient";
import { logger } from "../tsgf/logger";
import { ErrorCodes, IResult, Result } from "../tsgf/Result";
import { IChangeCustomPlayerProfile, IChangeCustomPlayerStatus, IChangePlayerTeam, IPlayerInfoPara, IPlayerInfo } from "../tsgf/player/IPlayerInfo";
import { ReqAuthorize } from "./protocols/PtlAuthorize";
import { IChangeRoomPara, IRoomInfo } from "../tsgf/room/IRoomInfo";
import { IRecvRoomMsg, IRoomMsg } from "../tsgf/room/IRoomMsg";
import { IAfterFrames, IGameSyncFrame, IPlayerInputOperate } from "../tsgf/room/IGameFrame";
import { IMatchParamsFromRoomAllPlayer, IMatchResult, IMatchPlayerResultWithServer } from "../tsgf/match/Models";

/**游戏服务器的通讯类型定义*/
export type gameServiceType = ServiceType;

/**
 * 基础的游戏服务器api的客户端封装
 */
export class GameClient extends AWsClient<gameServiceType>{

    protected _playerToken: string;
    public get playerToken(): string {
        return this._playerToken;
    }
    protected _playerId: string;
    public get playerId(): string {
        return this._playerId;
    }

    protected _currRoomInfo: IRoomInfo | null = null;
    /**当前所在的房间, 各种操作会自动维护本属性值为最新*/
    public get currRoomInfo(): IRoomInfo | null {
        return this._currRoomInfo;
    }
    protected set currRoomInfo(roomInfo: IRoomInfo | null) {
        this._currRoomInfo = roomInfo;
        this._currPlayerInfo = this._currRoomInfo?.playerList.find(p => p.playerId === this._playerId) ?? null;
    }

    protected _currPlayerInfo: IPlayerInfo | null = null;
    /**当前玩家信息对象*/
    public get currPlayerInfo(): IPlayerInfo | null {
        return this._currPlayerInfo;
    }

    /**是否启用断线重连*/
    public enabledReconnect: boolean = true;
    /**
     * 断线重连等待秒数
     */
    public reconnectWaitSec = 2;
    protected reconnectTimerHD: any;

    /**
     * [需启用断线重连:enabledReconnect]每次开始断线重连时触发, [reconnectWaitSec]秒后开始重连
     * @param currTryCount 已经重试了几次了, 首次断线重连则为0
     */
    public onReconnectStart?: (currTryCount: number) => void;
    /**
     * 彻底断开触发, 如下情况:
     * 1. 断开连接时没启用断线重连则触发
     * 2. 主动断开时触发, reason='ManualDisconnect'
     * 3. 断线重连失败并不再重连时触发, reason='ReconnectFailed'
     * 4. 认证失败时会断开连接, 同时触发, reason='AuthorizeFailed'
     * @param reason 断开原因
     */
    public onDisconnected?: (reason?: string) => void;
    /**当前玩家不管什么原因离开了房间(主动离开,主动解散,房间被解散等等),都会触发*/
    public onLeaveRoom?: (roomInfo: IRoomInfo) => void;
    /**当前玩家加入到房间后触发*/
    public onJoinRoom?: (roomInfo: IRoomInfo) => void;
    /**断线重连最终有结果时触发(终于连上了,或者返回不继续尝试了)*/
    public onReconnectResult?: (succ: boolean, err: string | null) => void;
    /**当接收到房间消息时触发*/
    public onRecvRoomMsg?: (roomMsg: IRecvRoomMsg) => void;
    /**【在房间中才能收到】玩家加入当前房间（自己操作的不触发）*/
    public onPlayerJoinRoom?: (player: IPlayerInfo, roomInfo: IRoomInfo) => void;
    /**【在房间中才能收到】玩家退出当前房间（自己操作的不触发）*/
    public onPlayerLeaveRoom?: (player: IPlayerInfo, roomInfo: IRoomInfo) => void;
    /**【在房间中才能收到】当前房间被解散（自己操作的不触发）*/
    public onDismissRoom?: (roomInfo: IRoomInfo) => void;
    /**【在房间中才能收到】房间中开始帧同步了*/
    public onStartFrameSync?: (roomInfo: IRoomInfo, startPlayer: IPlayerInfo) => void;
    /**【在房间中才能收到】房间中停止帧同步了*/
    public onStopFrameSync?: (roomInfo: IRoomInfo, stopPlayer: IPlayerInfo) => void;
    /**【在房间中才能收到】房间中收到一个同步帧*/
    public onRecvFrame?: (syncFrame: IGameSyncFrame) => void;
    /**【在房间中才能收到】服务端要求玩家上传状态同步数据 (调用 playerSendSyncState 方法)*/
    public onRequirePlayerSyncState?: () => void;
    /**【在房间中才能收到】玩家加入当前房间（自己操作的不触发）*/
    public onChangePlayerNetworkState?: (player: IPlayerInfo) => void;
    /**【在房间中才能收到】有玩家修改了自定义属性(只要在房间,自己也会收到)*/
    public onChangeCustomPlayerProfile?: (changeInfo: IChangeCustomPlayerProfile) => void;
    /**【在房间中才能收到】有玩家修改了自定义状态(只要在房间,自己也会收到)*/
    public onChangeCustomPlayerStatus?: (changeInfo: IChangeCustomPlayerStatus) => void;
    /**【在房间中才能收到】有玩家修改了自定义属性(只要在房间,自己也会收到)*/
    public onChangeRoom?: (roomInfo: IRoomInfo) => void;
    /**【在房间中才能收到】有玩家修改了所在队伍(只要在房间,自己也会收到)*/
    public onChangePlayerTeam?: (changeInfo: IChangePlayerTeam) => void;
    /**【在房间中才能收到】有玩家发起了全房间玩家匹配(自己也会收到)*/
    public onRoomAllPlayersMatchStart?: (matchReqId: string, req_playerId: string, matchParams: IMatchParamsFromRoomAllPlayer) => void;
    /**【在房间中才能收到】全房间玩家匹配有结果了(自己也会收到) */
    public onRoomAllPlayersMatchResult?: (errMsg?: string, errCode?: ErrorCodes, matchResult?: IMatchPlayerResultWithServer) => void;

    /**
     *
     * @param _playerToken 服务端调用大厅授权接口，获得玩家授权令牌
     * @param serverUrl
     */
    constructor(serverUrl: string, _playerToken: string) {
        super(gameServiceProto, {
            server: serverUrl,
            json: false,
            logger: logger,
        });
        this._playerToken = _playerToken;
        this._playerId = "";
        //设置断线重连的中间件
        this.client.flows.postDisconnectFlow.push(async v => {
            //如果都没连上过就断开,那么忽略
            if (!this._playerId) return v;

            //判断是否需要重连
            if (!v.isManual) {
                if (this.enabledReconnect) {
                    //启用断线重连
                    this.onReconnectStart?.call(this, 0);
                    this.client.logger?.error('连接已断开, 等待' + this.reconnectWaitSec + '秒后自动重连');
                    if (this.reconnectTimerHD) clearTimeout(this.reconnectTimerHD);
                    this.reconnectTimerHD = setTimeout(async () => this.startReconnect(0, true), this.reconnectWaitSec * 1000);
                    return v;
                }

                //被断开,并且没启用断线重连
                if (this.currRoomInfo) {
                    //如果被断开时,有在房间中,则先触发离开房间
                    this.onLeaveRoom?.call(this, this.currRoomInfo);
                }
                this.onDisconnected?.call(this, v.reason);
            } else {
                //主动断开
                this.onDisconnected?.call(this, v.reason ?? 'ManualDisconnect');
            }

            //确认彻底断开了,清理数据
            this.clearData();
            this.client.logger?.error('连接已断开');
            return v;
        });
        this.client.listenMsg("NotifyRoomMsg", (msg) => {
            this.onRecvRoomMsg?.call(this, msg.recvRoomMsg);
        });
        this.client.listenMsg("NotifyJoinRoom", (msg) => {
            this.currRoomInfo = msg.roomInfo;

            let joinPlayer = this.currRoomInfo.playerList.find(p => p.playerId === msg.joinPlayerId)!;
            this.onPlayerJoinRoom?.call(this, joinPlayer, this.currRoomInfo);
        });
        this.client.listenMsg("NotifyLeaveRoom", (msg) => {
            this.currRoomInfo = msg.roomInfo;

            this.onPlayerLeaveRoom?.call(this, msg.leavePlayerInfo, this.currRoomInfo);
        });
        this.client.listenMsg("NotifyDismissRoom", (msg) => {
            if (this.currRoomInfo) {
                this.onLeaveRoom?.call(this, this.currRoomInfo);
            }
            this.currRoomInfo = null;
            this.onDismissRoom?.call(this, msg.roomInfo);
        });
        this.client.listenMsg("NotifyStartFrameSync", (msg) => {
            this.currRoomInfo = msg.roomInfo;

            this.onStartFrameSync?.call(this, this.currRoomInfo,
                this.currRoomInfo.playerList.find(p => p.playerId === msg.startPlayerId)!);
        });
        this.client.listenMsg("NotifyStopFrameSync", (msg) => {
            this.currRoomInfo = msg.roomInfo;

            this.onStopFrameSync?.call(this, this.currRoomInfo,
                this.currRoomInfo.playerList.find(p => p.playerId === msg.stopPlayerId)!);
        });
        this.client.listenMsg("NotifySyncFrame", (msg) => {
            this.onRecvFrame?.call(this, msg.syncFrame);
        });
        this.client.listenMsg("RequirePlayerSyncState", (msg) => {
            this.onRequirePlayerSyncState?.call(this);
        });
        this.client.listenMsg("NotifyChangeRoom", (msg) => {
            this.currRoomInfo = msg.roomInfo;

            this.onChangeRoom?.call(this, this.currRoomInfo);
        });
        this.client.listenMsg("NotifyChangePlayerNetworkState", (msg) => {
            this.currRoomInfo = msg.roomInfo;

            let player = this.currRoomInfo.playerList.find(p => p.playerId === msg.changePlayerId)!;
            this.onChangePlayerNetworkState?.call(this, player);
        });
        this.client.listenMsg("NotifyChangeCustomPlayerProfile", (msg) => {
            this.currRoomInfo = msg.roomInfo;

            this.onChangeCustomPlayerProfile?.call(this, msg);
        });
        this.client.listenMsg("NotifyChangeCustomPlayerStatus", (msg) => {
            this.currRoomInfo = msg.roomInfo;

            this.onChangeCustomPlayerStatus?.call(this, msg);
        });
        this.client.listenMsg("NotifyChangePlayerTeam", (msg) => {
            this.currRoomInfo = msg.roomInfo;

            this.onChangePlayerTeam?.call(this, msg);
        });
        this.client.listenMsg("NotifyRoomAllPlayersMatchStart", (msg) => {
            this.currRoomInfo = msg.roomInfo;

            this.onRoomAllPlayersMatchStart?.call(this, msg.matchReqId, msg.reqPlayerId, msg.matchParams);
        });
        this.client.listenMsg("NotifyRoomAllPlayersMatchResult", (msg) => {
            this.currRoomInfo = msg.roomInfo;

            this.onRoomAllPlayersMatchResult?.call(this, msg.errMsg, msg.errCode, msg.matchResult);
        });
    }
    /**
     * Disconnects game client
     * @param reason websocket的关闭原因字符串,可自定义
     * @param code websocket的关闭原因代码, 取值范围: [1000,3000-4999]
     * @returns disconnect 
     */
    public async disconnect(reason: string = 'ManualDisconnect'): Promise<void> {
        this.stopReconnect();
        if (this._playerId || this.client.isConnected) {
            if (this.currRoomInfo) {
                //如果断开时,有在房间中,则先触发离开房间事件
                this.onLeaveRoom?.call(this, this.currRoomInfo);
            }
            this.clearData();
            await this.client.sendMsg("Disconnect", {});
            await this.client.disconnect(1000, reason);
        }
    }
    protected async clearData(): Promise<void> {
        this._playerId = '';
        this._playerToken = '';
        this._currRoomInfo = null;
        this._currPlayerInfo = null;
        this.onReconnectStart = undefined;
        this.onDisconnected = undefined;
        this.onReconnectResult = undefined;
        this.onLeaveRoom = undefined;
        this.onRecvRoomMsg = undefined;
        this.onPlayerJoinRoom = undefined;
        this.onPlayerLeaveRoom = undefined;
        this.onDismissRoom = undefined;
        this.onStartFrameSync = undefined;
        this.onStopFrameSync = undefined;
        this.onRecvFrame = undefined;
        this.onRequirePlayerSyncState = undefined;
        this.onChangePlayerNetworkState = undefined;
        this.onChangeCustomPlayerProfile = undefined;
        this.onChangeCustomPlayerStatus = undefined;
        this.onChangeRoom = undefined;
        this.onChangePlayerTeam = undefined;
        this.onRoomAllPlayersMatchStart = undefined;
        this.onRoomAllPlayersMatchResult = undefined;
    }


    protected stopReconnect(): void {
        if (this.reconnectTimerHD) {
            clearTimeout(this.reconnectTimerHD);
            this.reconnectTimerHD = null;
        }
    }
    /**
     * Starts reconnect
     * @param currTryCount 当前重试次数
     * @param failReTry 本次失败后是否继续重试
     * @returns reconnect 
     */
    protected async startReconnect(currTryCount: number = 0, failReTry = true): Promise<boolean> {
        const result = await this.reconnect();
        // 重连也错误，弹出错误提示
        if (result.succ) {
            this.client.logger?.log('重连成功!');
            this.onReconnectResult?.call(this, true, null);
            return true;
        }
        //如果是逻辑拒绝则不需要重连
        if (!this._playerToken || result.code == ErrorCodes.AuthReconnectionFail) failReTry = false;

        if (failReTry && this.enabledReconnect) {
            currTryCount++;
            this.onReconnectStart?.call(this, currTryCount);
            this.client.logger?.error('重连失败:' + result.err + ' ' + this.reconnectWaitSec + '秒后自动重连!');
            if (this.reconnectTimerHD) clearTimeout(this.reconnectTimerHD);
            this.reconnectTimerHD = setTimeout(() => this.startReconnect(currTryCount, failReTry), this.reconnectWaitSec * 1000);
        } else {
            this.client.logger?.error('重连失败:' + result.err);
            await this.disconnect('ReconnectFailed');
            this.onReconnectResult?.call(this, false, result.err);
        }
        return false;
    }
    /**
     * 断线重连, 失败的话要看code, ErrorCodes.AuthReconnectionFail 表示逻辑拒绝,不需要重连
     * @returns  
     */
    public async reconnect(): Promise<IResult<null>> {
        const connectRet = await this.client.connect();
        if (!connectRet.isSucc) {
            return Result.buildErr("连接失败:" + connectRet.errMsg);
        }
        const loginRet = await this.client.callApi("Reconnect", {
            playerToken: this._playerToken,
        });
        if (!loginRet.isSucc) {
            return Result.buildErr(loginRet.err.message, (loginRet.err?.code ?? 1) as number);
        }
        this.currRoomInfo = loginRet.res.currRoomInfo;
        return Result.buildSucc(null);
    }


    /**
     * 登录到游戏服务器, 失败则断开连接并清理数据
     * @param infoPara 
     * @returns  
     */
    public async authorize(infoPara?: IPlayerInfoPara): Promise<IResult<null>> {
        const connectRet = await this.client.connect();
        if (!connectRet.isSucc) {
            return Result.buildErr("连接失败:" + connectRet.errMsg);
        }
        let req: ReqAuthorize = (infoPara as ReqAuthorize) ?? ({} as ReqAuthorize);
        req.playerToken = this._playerToken;
        const loginRet = await this.client.callApi("Authorize", req);
        if (!loginRet.isSucc) {
            let errCode = (loginRet.err?.code ?? 1) as number;
            this.disconnect('AuthorizeFailed');
            return Result.buildErr(loginRet.err.message, errCode);
        }
        this._playerId = loginRet.res.playerInfo.playerId;
        return Result.buildSucc(null);
    }


    /**
     * 进房间
     * @param roomId 
     * @param teamId 同时加入指定队伍 
     * @returns  
     */
    public async joinRoom(roomId: string, teamId?: string): Promise<IResult<IRoomInfo>> {
        const ret = await this.client.callApi("JoinRoom", {
            roomId: roomId,
            teamId: teamId,
        });
        if (!ret.isSucc) {
            return Result.buildErr(ret.err.message, (ret.err?.code ?? 1) as number);
        }
        this.currRoomInfo = ret.res.roomInfo;
        this.onJoinRoom?.call(this, this.currRoomInfo);
        return Result.buildSucc(ret.res.roomInfo);
    }
    /**
     * 退出当前房间
     * @param retainEmptyRoom 保留空房间(默认为false),即离开后如果没人了是否保留本房间一段时间,用于后续再次进入,一般用于组队房间
     * @returns  
     */
    public async leaveRoom(retainEmptyRoom?: boolean): Promise<IResult<null>> {
        const ret = await this.client.callApi("LeaveRoom", {
            retainEmptyRoom: retainEmptyRoom,
        });
        if (!ret.isSucc) {
            return Result.buildErr(ret.err.message, (ret.err?.code ?? 1) as number);
        }
        if (this.currRoomInfo) {
            this.onLeaveRoom?.call(this, this.currRoomInfo);
        }
        this.currRoomInfo = null;
        return Result.buildSucc(null);
    }
    /**
     * 【仅房主】解散当前房间
     * @param roomId 
     * @returns  
     */
    public async dismissRoom(): Promise<IResult<IRoomInfo>> {
        if (!this.currRoomInfo) return Result.buildErr('当前不在房间中！');
        const ret = await this.client.callApi("DismissRoom", {
            roomId: this.currRoomInfo.roomId
        });
        if (!ret.isSucc) {
            return Result.buildErr(ret.err.message, (ret.err?.code ?? 1) as number);
        }
        if (this.currRoomInfo) {
            this.onLeaveRoom?.call(this, this.currRoomInfo);
        }
        this.currRoomInfo = null;
        return Result.buildSucc(ret.res.roomInfo);
    }

    /**
     * 修改房间信息(注意,只能房主操作),同时同步更新本地当前房间信息
     *
     * @param changePara
     * @returns
     */
    public async changeRoom(changePara: IChangeRoomPara): Promise<IResult<IRoomInfo>> {
        const ret = await this.client.callApi("ChangeRoom", changePara);
        if (!ret.isSucc) {
            return Result.buildErr(ret.err.message, (ret.err?.code ?? 1) as number);
        }
        this.currRoomInfo = ret.res.roomInfo;
        return Result.buildSucc(ret.res.roomInfo);
    }
    /**
     * 修改自己的玩家自定义属性,如果当前在房间中会同时会触发通知(房间中所有玩家)
     *
     * @param customPlayerProfile
     * @returns
     */
    public async changeCustomPlayerProfile(customPlayerProfile: string): Promise<IResult<null>> {
        const ret = await this.client.callApi("ChangeCustomPlayerProfile", {
            customPlayerProfile: customPlayerProfile,
        });
        if (!ret.isSucc) {
            return Result.buildErr(ret.err.message, (ret.err?.code ?? 1) as number);
        }
        if (this._currPlayerInfo) this._currPlayerInfo.customPlayerProfile = customPlayerProfile;
        return Result.buildSucc(null);
    }
    /**
     * 修改自己的玩家自定义状态,如果当前在房间中会同时会触发通知(房间中所有玩家)
     *
     * @param customPlayerStatus
     * @returns
     */
    public async changeCustomPlayerStatus(customPlayerStatus: number): Promise<IResult<null>> {
        const ret = await this.client.callApi("ChangeCustomPlayerStatus", {
            customPlayerStatus: customPlayerStatus,
        });
        if (!ret.isSucc) {
            return Result.buildErr(ret.err.message, (ret.err?.code ?? 1) as number);
        }
        if (this._currPlayerInfo) this._currPlayerInfo.customPlayerStatus = customPlayerStatus;
        return Result.buildSucc(null);
    }

    /**
     *变更自己所在队伍
     *
     * @param newTeamId 传undefined表示改为无队伍; 如果有指定队伍, 但房间不存在该队伍id, 则需要房间开启自由队伍选项
     * @returns 
     */
    public async changePlayerTeam(newTeamId?: string): Promise<IResult<null>> {
        const ret = await this.client.callApi("ChangePlayerTeam", {
            newTeamId: newTeamId,
        });
        if (!ret.isSucc) {
            return Result.buildErr(ret.err.message, (ret.err?.code ?? 1) as number);
        }
        this.currRoomInfo = ret.res.roomInfo;
        return Result.buildSucc(null);
    }


    /**
     * 发送房间消息（自定义消息），可以指定房间里的全部玩家或部分玩家或其他玩家
     *
     * @public
     * @param roomMsg 
     * @returns
     */
    public async sendRoomMsg(roomMsg: IRoomMsg): Promise<IResult<null>> {
        const ret = await this.client.callApi("SendRoomMsg", {
            roomMsg: roomMsg
        });
        if (!ret.isSucc) {
            return Result.buildErr(ret.err.message, (ret.err?.code ?? 1) as number);
        }
        return Result.buildSucc(null);
    }

    /**
     * 发送房间消息（自定义消息），可以指定房间里的全部玩家或部分玩家或其他玩家
     *
     * @public
     * @param roomMsg 
     * @returns
     */
    public async startFrameSync(): Promise<IResult<null>> {
        const ret = await this.client.callApi("StartFrameSync", {
        });
        if (!ret.isSucc) {
            return Result.buildErr(ret.err.message, (ret.err?.code ?? 1) as number);
        }
        return Result.buildSucc(null);
    }
    /**
     * 发送房间消息（自定义消息），可以指定房间里的全部玩家或部分玩家或其他玩家
     *
     * @public
     * @param roomMsg 
     * @returns
     */
    public async stopFrameSync(): Promise<IResult<null>> {
        const ret = await this.client.callApi("StopFrameSync", {
        });
        if (!ret.isSucc) {
            return Result.buildErr(ret.err.message, (ret.err?.code ?? 1) as number);
        }
        return Result.buildSucc(null);
    }

    /**
     * 发送玩家输入帧(加入到下一帧的操作列表)
     *
     * @public
     * @param inpOperates 
     * @returns
     */
    public async playerInpFrame(inpOperates: IPlayerInputOperate[]): Promise<IResult<null>> {
        const ret = await this.client.sendMsg("PlayerInpFrame", {
            operates: inpOperates
        });
        if (!ret.isSucc) {
            return Result.buildErr(ret.err.message, (ret.err?.code ?? 1) as number);
        }
        return Result.buildSucc(null);
    }
    /**
     * 请求追帧数据(当前的所有帧数据[+同步状态数据])
     *
     * @public
     * @returns
     */
    public async requestAfterFrames(): Promise<IResult<IAfterFrames>> {
        const ret = await this.client.callApi("RequestAfterFrames", {
        });
        if (!ret.isSucc) {
            return Result.buildErr(ret.err.message, (ret.err?.code ?? 1) as number);
        }
        return Result.buildSucc(ret.res);
    }
    /**
     * 自主请求帧数组
     *
     * @public
     * @param beginFrameIndex 起始帧索引(包含)
     * @param endFrameIndex 结束帧索引(包含)
     * @returns
     */
    public async requestFrames(beginFrameIndex: number, endFrameIndex: number): Promise<IResult<IGameSyncFrame[]>> {
        const ret = await this.client.callApi("RequestFrames", {
            beginFrameIndex: beginFrameIndex,
            endFrameIndex: endFrameIndex,
        });
        if (!ret.isSucc) {
            return Result.buildErr(ret.err.message, (ret.err?.code ?? 1) as number);
        }
        return Result.buildSucc(ret.res.frames);
    }

    /**
     * 玩家发送本地的同步状态数据(有启用状态同步的时候才可以用)
     *
     * @public
     * @param stateData
     * @param stateFrameIndex
     * @returns
     */
    public async playerSendSyncState(stateData: object, stateFrameIndex: number): Promise<IResult<null>> {
        const ret = await this.client.sendMsg("PlayerSendSyncState", {
            stateData: stateData,
            stateFrameIndex: stateFrameIndex,
        });
        if (!ret.isSucc) {
            return Result.buildErr(ret.err.message, (ret.err?.code ?? 1) as number);
        }
        return Result.buildSucc(null);
    }

    /**
     * 发起房间所有玩家匹配请求
     * 请求成功即返回,同时房间中的所有玩家会收到通知
     * 匹配有结果了还会收到消息通知, 并且可由一个玩家调用QueryMatch等待完整匹配结果
     *
     * @param matchParams
     * @returns 匹配请求id
     */
    public async requestMatch(matchParams: IMatchParamsFromRoomAllPlayer): Promise<IResult<string>> {
        const ret = await this.client.callApi("RequestMatch", {
            matchParams: matchParams
        });
        if (!ret.isSucc) {
            return Result.buildErr(ret.err.message, (ret.err?.code ?? 1) as number);
        }
        return Result.buildSucc(ret.res.matchReqId);
    }
    /**
     * 取消匹配请求
     * 可能发生并发,导致虽然请求成功了,但还是收到了成功结果的通知
     *
     * @returns 匹配请求id
     */
    public async cancelMatch(): Promise<IResult<null>> {
        const ret = await this.client.callApi("CancelMatch", {
        });
        if (!ret.isSucc) {
            return Result.buildErr(ret.err.message, (ret.err?.code ?? 1) as number);
        }
        return Result.buildSucc(null);
    }
    /**
     * 查询完整匹配结果
     * 会等到有结果了才返回!
     * 注意: 同时只能只有一个玩家进行查询等待,一般使用通知来获取结果即可
     *
     * @returns
     */
    public async queryMatch(): Promise<IResult<IMatchResult>> {
        const ret = await this.client.callApi("QueryMatch", {
        }, {
            timeout: 0
        });
        if (!ret.isSucc) {
            return Result.buildErr(ret.err.message, (ret.err?.code ?? 1) as number);
        }
        return Result.buildSucc(ret.res.matchResult);
    }

}