import { IRoomInfo } from "../../tsgf/room/IRoomInfo";

export interface ReqChangePlayerTeam {
    newTeamId?:string;
}

export interface ResChangePlayerTeam {
    roomInfo:IRoomInfo;
}
