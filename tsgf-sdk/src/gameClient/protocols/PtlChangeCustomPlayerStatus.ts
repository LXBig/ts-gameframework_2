
/**
 * 修改玩家自定义状态
 * 修改后房间内(如果在的话)所有玩家(包含自己)会收到通知
*/
export interface ReqChangeCustomPlayerStatus {
    customPlayerStatus:number;
}

export interface ResChangeCustomPlayerStatus {
    
}