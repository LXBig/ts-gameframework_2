import { IRoomInfo } from "../../tsgf/room/IRoomInfo";


/**
 * 加入房间
 * */
export interface ReqJoinRoom {
    roomId: string;
    /**同时指定加入的队伍ID*/
    teamId?: string;
}

export interface ResJoinRoom {
    roomInfo: IRoomInfo;
}