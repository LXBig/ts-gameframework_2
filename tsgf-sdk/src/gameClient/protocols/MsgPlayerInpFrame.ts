import { IPlayerInputOperate } from "../../tsgf/room/IGameFrame";

export interface MsgPlayerInpFrame {
    /**本帧本用户的操作列表*/
    operates: IPlayerInputOperate[];
}
