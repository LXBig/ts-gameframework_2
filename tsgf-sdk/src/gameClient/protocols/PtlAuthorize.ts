import { IPlayerInfoPara, IPlayerInfo } from "../../tsgf/player/IPlayerInfo";

/**
 * 玩家认证
 * 需要连接后立即发出请求,否则超时将被断开连接
 * */
export interface ReqAuthorize extends IPlayerInfoPara{
    /**玩家令牌,登录大厅时获得的 */
    playerToken: string;
}

export interface ResAuthorize {
    /**玩家ID*/
    playerInfo: IPlayerInfo;
}