import { IPlayerInfo } from "../../tsgf/player/IPlayerInfo";


/**玩家认证信息*/
export interface IPlayerAuthInfo {
    /**全局唯一的玩家ID*/
    playerId: string;
    /**所属应用ID*/
    appId: string;
    /**应用下的玩家ID（应用自定义玩家ID）*/
    openId: string;
    /**玩家认证令牌*/
    playerToken: string;
    /**显示名称*/
    showName: string;
    /**是否已经失效，一般发生在同玩家在token还没失效的情况下，重复授权，会把之前的token设置为失效状态*/
    invalid: boolean;
    /**过期时间(毫秒级时间戳)*/
    expireDate: number;
}

export interface IPlayer {
    authInfo: IPlayerAuthInfo;
    playerInfo: IPlayerInfo;
    /**当前所在的房间ID*/
    currRoomId?: string;
}