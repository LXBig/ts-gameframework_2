import { ApiCall, BaseCall, BaseConnection, BaseServer, BaseServiceType, FlowNode, MsgCall, TsrpcError, WsServer, WsServerOptions } from "tsrpc";
import { apiErrorThenClose } from "../ApiBase";
import { MsgClusterSyncNodeInfo } from "./protocols/MsgClusterSyncNodeInfo";
import { MsgAssignTask } from "./protocols/MsgAssignTask";
import { ReqClusterLogin, ResClusterLogin } from "./protocols/PtlClusterLogin";
import { ServiceType as ClusterServiceType, serviceProto as clusterServiceProto } from "./protocols/serviceProto";
import { RedisClient } from "../redisHelper";
import { ErrorCodes, IResult, Result } from "../../tsgf/Result";


declare module 'tsrpc' {
    export interface BaseConnection {
        nodeId: string;
    }
}

/**集群节点*/
export interface IClusterNode<NodeInfo> {
    nodeInfo: IClusterNodeInfo<NodeInfo>;
    nodeConn: BaseConnection<ClusterServiceType>;
}
/**集群节点信息*/
export interface IClusterNodeInfo<NodeInfo> {
    clusterNodeId: string;
    info: NodeInfo;
    /**过期时间(毫秒级时间戳)*/
    expires: number;
}
/**集群节点配置*/
export interface IClusterNodeCfg {
    clusterNodeId: string;
    clusterKey: string;
}


/**
 * 集群管理类
 * @date 2022/4/19 - 16:48:53
 *
 * @class ClusterMgr
 * @typedef {ClusterMgr}
 * @typeParam NodeInfo 节点信息的类型，可自定义
 */
export class ClusterMgr<NodeInfo = any>{

    /**获取所有可以加入集群的配置 */
    private getNodesCfg: () => IClusterNodeCfg[];

    /**集群类型标识*/
    protected clusterTypeKey: string;

    /**服务*/
    protected server: WsServer<ClusterServiceType>;

    /**所有节点，nodeId=>IClusterNode*/
    protected nodes: Map<string, IClusterNode<NodeInfo>> = new Map<string, IClusterNode<NodeInfo>>();

    protected getRedisClient: () => Promise<RedisClient>;

    protected onNodeConnected?: (node: IClusterNode<NodeInfo>) => void;
    protected onNodeDisconnected?: (nodeId: string) => void;

    private static buildNodesHashKey(clusterTypeKey: string) {
        return `ClusterMgr:${clusterTypeKey}:Nodes`;
    }

    /**
     * Creates an instance of ClusterMgr.
     *
     * @param clusterTypeKey 集群类型标识，用在各种场合进行区分的，需要唯一定义
     * @param getNodesCfg
     * @param serverOption
     */
    constructor(
        clusterTypeKey: string, getNodesCfg: () => IClusterNodeCfg[],
        serverOption: Partial<WsServerOptions<ClusterServiceType>>,
        getRedisClient: () => Promise<RedisClient>) {
        this.clusterTypeKey = clusterTypeKey;
        this.getNodesCfg = getNodesCfg;
        this.getRedisClient = getRedisClient;
        this.server = new WsServer(clusterServiceProto, serverOption);

        //所有消息和api请求，都必须在认证通过之后
        this.server.flows.preApiCallFlow.push((call) => {
            if (!this.prefixCheck(call)) {
                apiErrorThenClose(call, 'need Login before do this', { code: ErrorCodes.AuthUnverified });
                return;
            }
            return call;
        });
        this.server.flows.preMsgCallFlow.push((call) => {
            if (!this.prefixCheck(call)) {
                call.logger.error(`need Login before do this (${call.service.name}, msg:${JSON.stringify(call.msg)})`);
                call.conn.close();
                return;
            }
            return call;
        });
        this.server.implementApi("ClusterLogin", async (call) => {
            await this.apiLogin(call);
        });
        this.server.listenMsg("ClusterSyncNodeInfo", async call => {
            await this.msgSyncNodeInfo(call);
        });
        this.server.flows.postDisconnectFlow.push((v) => {
            this.nodeDisconnect(v.conn);
            return v;
        });
    }

    public async start() {
        await this.server.start();
    }
    public async stop() {
        await this.server.stop();
    }

    private prefixCheck(call: BaseCall<ClusterServiceType>): boolean {
        if (call.service.name == "ClusterLogin") {
            return true;
        }
        if (!call.conn.nodeId) {
            return false;
        }
        return true;
    }
    private async apiLogin(call: ApiCall<ReqClusterLogin, ResClusterLogin, any>) {
        let cfg = this.getNodesCfg().find(c => c.clusterNodeId === call.req.nodeId);
        if (!cfg) {
            return await apiErrorThenClose(call, `认证失败！不存在nodeId=${call.req.nodeId}`);
        }
        if (cfg.clusterKey !== call.req.clusterKey) {
            return await apiErrorThenClose(call, `认证失败！错误的clusterKey`);
        }
        cfg = undefined;

        let node: IClusterNode<NodeInfo> = {
            nodeInfo: {
                clusterNodeId: call.req.nodeId,
                info: call.req.nodeInfo,
                expires: Date.now() + 3 * 60 * 1000,//3分钟过期时间
            },
            nodeConn: call.conn,
        };
        this.nodes.set(node.nodeInfo.clusterNodeId, node);
        this.syncNodeInfoToRedis(node.nodeInfo);

        call.conn.nodeId = node.nodeInfo.clusterNodeId;

        await call.succ({});

        this.onNodeConnected?.call(this, node);
    }
    private async msgSyncNodeInfo(call: MsgCall<MsgClusterSyncNodeInfo, any>) {

        let node = this.nodes.get(call.conn.nodeId);
        if (!node) {
            call.logger.error(`连接错误将被踢出`);
            call.conn.close();
            return;
        }

        node.nodeInfo.info = call.msg.nodeInfo;
        node.nodeInfo.expires = Date.now() + 3 * 60 * 1000,//3分钟过期时间
        this.syncNodeInfoToRedis(node.nodeInfo);
    }
    private nodeDisconnect(conn: BaseConnection<ClusterServiceType>) {
        this.nodes.delete(conn.nodeId);
        this.delNodeInfoToRedis(conn.nodeId);

        this.onNodeDisconnected?.call(this, conn.nodeId);
    }

    private async syncNodeInfoToRedis(nodeInfo: IClusterNodeInfo<NodeInfo>) {
        await (await this.getRedisClient()).setHashObject(
            ClusterMgr.buildNodesHashKey(this.clusterTypeKey),
            nodeInfo.clusterNodeId, nodeInfo);
    }
    private async delNodeInfoToRedis(nodeId: string) {
        await (await this.getRedisClient()).removeHashValue(`ClusterMgr:${this.clusterTypeKey}:Nodes`, nodeId);
    }


    /**
     * 从redis中获取所有节点列表, 分布式时，大厅服务器和游戏服务器管理节点，可能不是同个实例，所以使用本方法来获取
     *
     * @public
     * @typeParam NodeInfo
     * @param clusterTypeKey 集群类型标识，用在各种场合进行区分的。需要和构造ClussterMgr时的值一致
     * @returns
     */
    public static async getNodeInfosFromRedis<NodeInfo>(
        clusterTypeKey: string,
        getRedisClient: (reuseClient: boolean) => Promise<RedisClient>): Promise<IClusterNodeInfo<NodeInfo>[]> {
        let hashKey = ClusterMgr.buildNodesHashKey(clusterTypeKey);
        let allKv = await (await getRedisClient(true))
            .getHashObjects<IClusterNodeInfo<NodeInfo>>(hashKey);
        let nodes: IClusterNodeInfo<NodeInfo>[] = [];
        let now = Date.now();
        for (let key in allKv) {
            let nodeInfo = allKv[key];
            if (nodeInfo.expires < now) continue;
            nodes.push(nodeInfo);
        }
        return nodes;
    }
    /**
     * 从redis中获取指定节点信息, 分布式时，大厅服务器和游戏服务器管理节点，可能不是同个实例，所以使用本方法来获取
     *
     * @public
     * @typeParam NodeInfo
     * @param clusterTypeKey 集群类型标识，用在各种场合进行区分的。需要和构造ClussterMgr时的值一致
     * @param clusterNodeId 节点ID
     * @returns
     */
    public static async getNodeInfoFromRedis<NodeInfo>(
        clusterTypeKey: string,
        clusterNodeId: string,
        getRedisClient: (reuseClient: boolean) => Promise<RedisClient>): Promise<IClusterNodeInfo<NodeInfo> | null> {

        let hashKey = ClusterMgr.buildNodesHashKey(clusterTypeKey);
        let node = await (await getRedisClient(true))
            .getHashObject<IClusterNodeInfo<NodeInfo>>(hashKey, clusterNodeId);
        if (!node) return null;
        if (node.expires < Date.now()) return null;
        return node;
    }


    /**
     * 给节点分配任务
     *
     * @public
     * @typeParam T
     * @param nodeId
     * @param taskId 任务ID需要集群内唯一
     * @param taskData
     * @returns
     */
    public async assignTask<T>(nodeId: string, taskId: string, taskData: T): Promise<IResult<null>> {
        let node = this.nodes.get(nodeId);
        if (!node) return Result.buildErr(`nodeId=${nodeId}不存在！`);
        let ret = await node.nodeConn.sendMsg('AssignTask', {
            taskId: taskId,
            taskData: taskData,
        });
        if (!ret.isSucc) return Result.buildErr(ret.errMsg);
        return Result.buildSucc(null);
    }
    /**
     * 给节点取消已经分配的任务
     *
     * @public
     * @typeParam T
     * @param nodeId
     * @param taskId 任务ID需要集群内唯一
     * @param taskData
     * @returns
     */
    public async cancelTask(nodeId: string, taskId: string): Promise<IResult<null>> {
        let node = this.nodes.get(nodeId);
        if (!node) return Result.buildErr(`节点ID${nodeId}不存在！`);
        let ret = await node.nodeConn.sendMsg('CancelTask', {
            taskId: taskId,
        });
        if (!ret.isSucc) return Result.buildErr(ret.errMsg);
        return Result.buildSucc(null);
    }

}

