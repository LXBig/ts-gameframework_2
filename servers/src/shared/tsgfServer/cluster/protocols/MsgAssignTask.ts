/**管理分配任务给节点*/
export interface MsgAssignTask {
    /**任务唯一ID*/
    taskId:string;
    /**任务数据*/
    taskData:any;
}
