import { ITeamPlayerIds } from "../../tsgf/room/IRoomInfo";
import { arrRemoveItems } from "../../tsgf/Utils";


/**服务器的房间注册信息，由大厅注册分配给游戏服务器使用，用于大厅能查询到房间ID关联的游戏服务器*/
export interface IRoomRegInfoInServer {
    /**房间ID，全局唯一*/
    roomId: string;
    /**所属应用id*/
    appId: string;
    /**所属玩家ID*/
    ownerPlayerId: string;
    /**挂在哪个游戏服务器下（服务器节点id）*/
    gameServerNodeId: string;
    /**创建时间*/
    createTime: number;
    /**当前队伍玩家id列表,如果没有队伍则[{teamId='',playerIds:[...所有玩家id都在这...]}]*/
    teamsPlayerIds: ITeamPlayerIds[];
}


/**
 * 给队伍玩家数组添加另一个队伍玩家数组, 结果放在 targetTeams 中
 *
 * @param targetTeams
 * @param add
 */
export function teamPlayerIdsAdd(targetTeams: ITeamPlayerIds[], add: ITeamPlayerIds[]) {
    for (let team of add) {
        let targetTeam = targetTeams.find(t => t.teamId === team.teamId ?? '');
        if (!targetTeam) {
            targetTeam = { teamId: team.teamId ?? '', playerIds: team.playerIds.slice() };
            targetTeams.push(targetTeam);
        } else {
            for (let appPid of team.playerIds) {
                if (!targetTeam.playerIds.includes(appPid)) {
                    targetTeam.playerIds.push(appPid);
                }
            }
        }
    }
}
/**
 * 给队伍玩家数组添加一个单个队伍玩家, 结果放在 targetTeams 中
 *
 * @param targetTeams
 * @param addTeamId 如果无队伍则会使用''
 * @param addPlayerId
 */
export function teamPlayerIdsAddSingle(targetTeams: ITeamPlayerIds[], addPlayerId: string, addTeamId: string | undefined) {
    if (!addTeamId) addTeamId = '';
    let targetTeam = targetTeams.find(t => t.teamId === addTeamId);
    if (!targetTeam) {
        targetTeam = { teamId: addTeamId, playerIds: [addPlayerId] };
        targetTeams.push(targetTeam);
    } else {
        if (!targetTeam.playerIds.includes(addPlayerId)) {
            targetTeam.playerIds.push(addPlayerId);
        }
    }
}

/**
 * 从一个队伍玩家数组中移除另一个队伍玩家数组的数据
 *
 * @param targetTeams
 * @param subtract
 * @param removeEmptyTeam=true
 */
export function teamPlayerIdsSubtract(targetTeams: ITeamPlayerIds[], subtract: ITeamPlayerIds[], removeEmptyTeam: boolean = true) {
    for (let team of subtract) {
        let targetTeamI = targetTeams.findIndex(t => t.teamId === team.teamId ?? '');
        let targetTeam = targetTeams[targetTeamI];
        if (targetTeam) {
            for (let appPid of team.playerIds) {
                arrRemoveItems(targetTeam.playerIds, p => p === appPid);
            }
            if (removeEmptyTeam && targetTeam.playerIds.length <= 0) {
                //这个队伍下的玩家都没了直接删除
                targetTeams.splice(targetTeamI, 1);
            }
        }
    }
}
/**
 *从一个队伍玩家数组中移除一个单个的队伍玩家
 *
 * @param targetTeams
 * @param subtractTeamId 如果无队伍则会使用''
 * @param subtractPlayerId
 * @param removeEmptyTeam=true
 */
export function teamPlayerIdsSubtractSingle(targetTeams: ITeamPlayerIds[], subtractPlayerId: string, subtractTeamId: string | undefined, removeEmptyTeam: boolean = true) {
    let targetTeamI = targetTeams.findIndex(t => t.teamId === subtractTeamId ?? '');
    let targetTeam = targetTeams[targetTeamI];
    if (targetTeam) {
        arrRemoveItems(targetTeam.playerIds, p => p === subtractPlayerId);
        if (removeEmptyTeam && targetTeam.playerIds.length <= 0) {
            //这个队伍下的玩家都没了直接删除
            targetTeams.splice(targetTeamI, 1);
        }
    }
}