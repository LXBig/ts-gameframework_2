import { v4 } from "uuid";

export function buildGuid(type?: string) {
    return (type ?? '') + v4();
}