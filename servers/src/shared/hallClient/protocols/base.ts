


/*============无加密=============*/
import { EApiCryptoMode } from "../../tsgf/apiCrypto/Models";

/**请求基类*/
export interface BaseRequest {
    /**有需要鉴权的接口,则需要传递玩家token*/
    playerToken?: string;
}
/**响应基类*/
export interface BaseResponse {
}

/**接口配置信息*/
export interface BaseConf {
    /**本接口是否跳过身份认证，没设置则为不跳过*/
    skipAuth?: boolean;
    /**加密的模式*/
    cryptoMode: EApiCryptoMode;
}