
/**
 * 离开房间
 * 
 * */
export interface ReqLeaveRoom {
    /**保留空房间(默认为false),即离开后如果没人了是否保留本房间一段时间,用于后续再次进入,一般用于组队房间*/
    retainEmptyRoom?:boolean;
}

export interface ResLeaveRoom {
}