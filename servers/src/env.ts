
import { BaseHttpClient, BaseHttpClientOptions, BaseServiceType, BaseWsClient, BaseWsClientOptions, HttpClient, ServiceProto, WsClient } from "tsrpc";
import { initSDKProvider, SDKProvider } from "./shared/tsgf/Provider";


initSDKProvider({
    env: {
        getHttpClient: (proto, options) => {
            return new HttpClient(proto, options);
        },
        getWsClient: (proto, options) => {
            return new WsClient(proto, options);
        },
    }
});