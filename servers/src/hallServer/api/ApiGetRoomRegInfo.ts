import { RoomHelper } from "../../shared/tsgfServer/room/RoomHelper";
import { ReqGetRoomRegInfo, ResGetRoomRegInfo } from "../../shared/hallClient/protocols/PtlGetRoomRegInfo";
import { HallApiCall } from "../HallServer";
import { ErrorCodes } from "../../shared/tsgf/Result";

export async function ApiGetRoomRegInfo(call: HallApiCall<ReqGetRoomRegInfo, ResGetRoomRegInfo>) {
    let regInfo = await RoomHelper.getRoomRegInfo(call.req.roomId);
    if (!regInfo) {
        return await call.error('房间不存在！', { code: ErrorCodes.RoomNotFound });
    }
    let gameServer = await call.getHallServer().getGameServer(regInfo.gameServerNodeId);
    if (!gameServer) {
        return await call.error('房间服务器已经关闭，请重新创建房间！', { code: ErrorCodes.RoomServerClosed });
    }
    await call.succ({
        regInfo: {
            gameServerUrl: gameServer.serverUrl,
        }
    });
}