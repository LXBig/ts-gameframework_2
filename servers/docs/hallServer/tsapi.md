
# TSRPC API 接口文档

## 通用说明

- 所有请求方法均为 `POST`
- 所有请求均需加入以下 Header :
    - `Content-Type: application/json`

## 目录

- [玩家获取认证信息(由应用web服务器在服务端调用)](#/Authorize)
- [取消匹配](#/CancelMatch)
- [创建房间](#/CreateRoom)
- [获取房间注册信息](#/GetRoomRegInfo)
- [查询匹配](#/QueryMatch)
- [请求匹配](#/RequestMatch)

---

## 玩家获取认证信息(由应用web服务器在服务端调用) <a id="/Authorize"></a>

**路径**
- POST `/Authorize`

**请求**
```ts
interface ReqAuthorize {
    /** 应用ID */
    appId: string,
    /** 请求数据密文 */
    ciphertext?: string,
    /** 请求数据对象, 到了ApiCall层就是解析通过可以使用的 */
    data?: any
}
```

**响应**
```ts
interface ResAuthorize {
    /** 平台生成的玩家ID */
    playerId: string,
    /** 所有需要认证的接口、服务器，都需要附带token */
    playerToken: string
}
```

**配置**
```ts
{
  "skipAuth": true,
  "cryptoMode": "AppReqDes"
}
```

---

## 取消匹配 <a id="/CancelMatch"></a>

**路径**
- POST `/CancelMatch`

**请求**
```ts
interface ReqCancelMatch {
    /** 非房间发起的匹配，发起匹配请求中的所有玩家都可以取消 */
    matchReqId: string,
    /** 有需要鉴权的接口,则需要传递玩家token */
    playerToken?: string
}
```

**响应**
```ts
interface ResCancelMatch {

}
```

**配置**
```ts
{
  "cryptoMode": "None"
}
```

---

## 创建房间 <a id="/CreateRoom"></a>

**路径**
- POST `/CreateRoom`

**请求**
```ts
interface ReqCreateRoom {
    /** 有需要鉴权的接口,则需要传递玩家token */
    playerToken?: string,
    /** 房间名字，查询房间和加入房间时会获取到 */
    roomName: string,
    /** 房主玩家ID，创建后，只有房主玩家的客户端才可以调用相关的管理操作, 如果不想任何人操作,可以直接设置为'', 所有人都离开房间后自动解散 */
    ownerPlayerId: string,
    /** 进入房间的最大玩家数量 */
    maxPlayers: number,
    /** 是否私有房间，即不参与匹配, 但可以通过房间ID加入 */
    isPrivate: boolean,
    /** 如果参与匹配,则使用的匹配器标识 */
    matcherKey?: string,
    /** 自定义房间属性字符串 */
    customProperties?: string,
    /** 房间类型，查询房间信息时可以获取到 */
    roomType?: string
}
```

**响应**
```ts
interface ResCreateRoom {
    /** 游戏服务器的连接地址 */
    gameServerUrl: string,
    /** 房间信息 */
    roomInfo: {
        /** 房间ID */
        roomId: string,
        /** 房间名称 */
        roomName: string,
        /** 房主玩家ID，创建后，只有房主玩家的客户端才可以调用相关的管理操作 */
        ownerPlayerId: string,
        /** 是否私有房间，即不参与匹配, 但可以通过房间ID加入 */
        isPrivate: boolean,
        /** 如果参与匹配,则使用的匹配器标识 */
        matcherKey?: string,
        /** 是否不允许加人 */
        isForbidJoin: boolean,
        /** 创建房间的方式 */
        createType: 0 | 1,
        /** 进入房间的最大玩家数量 */
        maxPlayers: number,
        /** 房间类型字符串 */
        roomType?: string,
        /** 自定义房间属性字符串 */
        customProperties?: string,
        /** 玩家列表 */
        playerList: {
            /** 玩家ID */
            playerId: string,
            /** 显示名 */
            showName: string,
            /** 当前所在队伍id */
            teamId?: string,
            /** 自定义玩家状态 */
            customPlayerStatus: number,
            /** 自定义玩家信息 */
            customProfile?: string,
            /** 网络状态 */
            networkState: 0 | 1,
            /** 是否机器人 */
            isRobot: boolean
        }[],
        /** 帧率 */
        frameRate: number,
        /** 帧同步状态 */
        frameSyncState: 0 | 1,
        /** 创建房间时间戳（单位毫秒， new Date(createTime) 可获得时间对象） */
        createTime: number,
        /** 开始游戏时间戳（单位毫秒， new Date(createTime) 可获得时间对象）,0表示未开始 */
        startGameTime: number
    }
}
```

**配置**
```ts
{
  "cryptoMode": "None"
}
```

---

## 获取房间注册信息 <a id="/GetRoomRegInfo"></a>

**路径**
- POST `/GetRoomRegInfo`

**请求**
```ts
interface ReqGetRoomRegInfo {
    roomId: string,
    /** 有需要鉴权的接口,则需要传递玩家token */
    playerToken?: string
}
```

**响应**
```ts
interface ResGetRoomRegInfo {
    regInfo: {/** 游戏服务器的连接地址 */
        gameServerUrl: string
    }
}
```

**配置**
```ts
{
  "cryptoMode": "None"
}
```

---

## 查询匹配 <a id="/QueryMatch"></a>

**路径**
- POST `/QueryMatch`

**请求**
```ts
interface ReqQueryMatch {
    /** 匹配请求ID，用于查询匹配结果，建议2秒调用查询一次，直到超时(因为请求时超时时间已知，客户端要加个超时判断) */
    matchReqId: string,
    /** 有需要鉴权的接口,则需要传递玩家token */
    playerToken?: string
}
```

**响应**
```ts
interface ResQueryMatch {
    /** 当前匹配是否有结果 */
    hasResult: boolean,
    /** 如果匹配结果是失败的则有错误消息 */
    errMsg?: string,
    /** 如果匹配结果是失败的则有错误码 */
    errCode?: number,
    /** 匹配结果, 如果匹配有结果并且是成功的，则不为空 */
    matchResult?: {
        roomId: string,
        gameServerUrl: string
    }
}
```

**配置**
```ts
{
  "cryptoMode": "None"
}
```

---

## 请求匹配 <a id="/RequestMatch"></a>

**路径**
- POST `/RequestMatch`

**请求**
```ts
interface ReqRequestMatch {
    /** 匹配参数 */
    matchParams: {
        matchFromType: "Player",
        /** 匹配发起的玩家信息 */
        matchFromInfo: {/** 要匹配的玩家ID */
            playerIds: string[]
        },
        /** 匹配器标识，内置的匹配器: MatcherKeys，也可以使用自定义的匹配器 */
        matcherKey: string,
        /** 匹配器参数，对应匹配器的参数配置 */
        matcherParams: any,
        /** 匹配超时秒数, 0或者undefined则不做超时限制 */
        matchTimeoutSec?: number,
        /** 房间最大玩家数, 只有相同的才会匹配在一起,并以此创建房间或者加入到一个一样值的可匹配房间 */
        maxPlayers: number
    } | {
        matchFromType: "RoomJoinUs",
        /** 匹配发起的玩家信息 */
        matchFromInfo: {/** 要人的房间ID */
            roomId: string
        },
        /** 匹配器标识，内置的匹配器: MatcherKeys，也可以使用自定义的匹配器 */
        matcherKey: string,
        /** 匹配器参数，对应匹配器的参数配置 */
        matcherParams: any,
        /** 匹配超时秒数, 0或者undefined则不做超时限制 */
        matchTimeoutSec?: number,
        /** 房间最大玩家数, 只有相同的才会匹配在一起,并以此创建房间或者加入到一个一样值的可匹配房间 */
        maxPlayers: number
    },
    /** 有需要鉴权的接口,则需要传递玩家token */
    playerToken?: string
}
```

**响应**
```ts
interface ResRequestMatch {
    /** 匹配请求ID，用于查询匹配结果，建议2秒调用查询一次，直到超时 */
    matchReqId: string
}
```

**配置**
```ts
{
  "cryptoMode": "None"
}
```

