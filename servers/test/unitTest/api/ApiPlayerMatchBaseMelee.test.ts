
import { delay } from "../../../src/shared/tsgf/Utils";
import { assert } from 'chai';
import { authPlayerToken, cancelMatch, createAndEnterRoom, hallClient, joinRoom, joinRoomUseGameServer, queryMatch, requestMatchOneSingle } from "./ApiUtils";
import { ERoomMsgRecvType } from "../../../src/shared/tsgf/room/IRoomMsg";
import { EMatchFromType, IMatchPlayerResultWithServer, ISingleMatcherParams, MatcherKeys } from "../../../src/shared/tsgf/match/Models";
import { ErrorCodes } from "../../../src/shared/tsgf/Result";


test('多个单元测试,因为防止并行,所以都放在一个文件', async function () {
    //注意:同个匹配器的,需要串行执行,防止并行带来的结果非预期


    //4max,min2,2人匹配,应成功,再进入招人匹配,依次进入2人,都要成功,再进1人应该要没结果,再取消!
    {
        //==========这里模拟服务端获取playerToken (openid需要按单元测试名区分一下,防止多个单元测试并行时token互踢)
        const auth1 = await authPlayerToken("zum0001_ApiPlayerMatch_4Max2Min2P_2P_1PNoResult", "zum1");
        const auth2 = await authPlayerToken("zum0002_ApiPlayerMatch_4Max2Min2P_2P_1PNoResult", "zum2");
        const auth3 = await authPlayerToken("zum0003_ApiPlayerMatch_4Max2Min2P_2P_1PNoResult", "zum3");
        const auth4 = await authPlayerToken("zum0004_ApiPlayerMatch_4Max2Min2P_2P_1PNoResult", "zum4");
        const auth5 = await authPlayerToken("zum0005_ApiPlayerMatch_4Max2Min2P_2P_1PNoResult", "zum5");
        //==========end
        let playerToken1 = auth1.playerToken;
        let playerId1 = auth1.playerId;
        let playerToken2 = auth2.playerToken;
        let playerId2 = auth2.playerId;
        let playerToken3 = auth3.playerToken;
        let playerId3 = auth3.playerId;
        let playerToken4 = auth4.playerToken;
        let playerId4 = auth4.playerId;
        let playerToken5 = auth5.playerToken;
        let playerId5 = auth5.playerId;

        //匹配请求
        let matchReqId1 = await requestMatchOneSingle(playerToken1, playerId1, 4, 2);
        let matchReqId2 = await requestMatchOneSingle(playerToken2, playerId2, 4, 2);
        await delay(1000);
        let matchRet1 = await queryMatch(playerToken1, matchReqId1);
        let matchRet2 = await queryMatch(playerToken2, matchReqId2);

        console.log('ApiPlayerMatch_4Max2Min2P_2P_1PNoResult, roomId:', matchRet1.roomId);

        let gameClient1 = await joinRoomUseGameServer(matchRet1.gameServerUrl, playerToken1, matchRet1.roomId, 'zum1');
        let gameClient2 = await joinRoomUseGameServer(matchRet2.gameServerUrl, playerToken2, matchRet2.roomId, 'zum2');

        let matchReqId3 = await requestMatchOneSingle(playerToken3, playerId3, 4, 2);
        await delay(1000);
        let matchRet3 = await queryMatch(playerToken3, matchReqId3);
        let gameClient3 = await joinRoomUseGameServer(matchRet3.gameServerUrl, playerToken3, matchRet3.roomId, 'zum3');

        let matchReqId4 = await requestMatchOneSingle(playerToken4, playerId4, 4, 2);
        await delay(1000);
        let matchRet4 = await queryMatch(playerToken4, matchReqId4);
        let gameClient4 = await joinRoomUseGameServer(matchRet4.gameServerUrl, playerToken4, matchRet4.roomId, 'zum4');

        let matchReqId5 = await requestMatchOneSingle(playerToken5, playerId5, 4, 2);
        await delay(1000);
        let retM5 = await hallClient.queryMatch(playerToken5, matchReqId5);
        assert.ok(retM5 === null, "应该没结果!实际有了!"+ JSON.stringify(retM5));
        //取消匹配
        await cancelMatch(playerToken5, matchReqId5);

        //这个时候退出了一个
        await gameClient4.leaveRoom();

        //玩家5这个时候要能匹配进房间才对!
        matchReqId5 = await requestMatchOneSingle(playerToken5, playerId5, 4, 2);
        await delay(1000);
        let matchRet5 = await queryMatch(playerToken5, matchReqId5);
        let gameClient5 = await joinRoomUseGameServer(matchRet5.gameServerUrl, playerToken5, matchRet5.roomId, 'zum5');


        await gameClient1.disconnect();
        await gameClient2.disconnect();
        await gameClient3.disconnect();
        await gameClient4.disconnect();
        await gameClient5.disconnect();

    }


    //模拟三个客户端单人混战匹配
    {
        //==========这里模拟服务端获取playerToken (openid需要按单元测试名区分一下,防止多个单元测试并行时token互踢)
        const auth1 = await authPlayerToken("zum0001_ApiPlayer_MatchSingle3~8", "zum1");
        const auth2 = await authPlayerToken("zum0002_ApiPlayer_MatchSingle3~8", "zum2");
        const auth3 = await authPlayerToken("zum0003_ApiPlayer_MatchSingle3~8", "zum3");
        //==========end
        let playerToken1 = auth1.playerToken;
        let playerId1 = auth1.playerId;
        let playerToken2 = auth2.playerToken;
        let playerId2 = auth2.playerId;
        let playerToken3 = auth3.playerToken;
        let playerId3 = auth3.playerId;

        //匹配请求
        let matchReqId1 = await requestMatchOneSingle(playerToken1, playerId1, 8, 3);
        let matchReqId2 = await requestMatchOneSingle(playerToken2, playerId2, 8, 3);
        let matchReqId3 = await requestMatchOneSingle(playerToken3, playerId3, 8, 3);

        //延时1秒
        await delay(1000);

        let matchRet1 = await queryMatch(playerToken1, matchReqId1);
        let matchRet2 = await queryMatch(playerToken2, matchReqId2);
        let matchRet3 = await queryMatch(playerToken3, matchReqId3);

        console.log('matchRet1', matchRet1);
        console.log('matchRet2', matchRet2);
        console.log('matchRet3', matchRet3);
        console.log('ApiPlayer_MatchSingle3~8, roomId:', matchRet1.roomId);

        let gameClient1 = await joinRoomUseGameServer(matchRet1.gameServerUrl, playerToken1, matchRet1.roomId, 'zum1');
        let gameClient2 = await joinRoomUseGameServer(matchRet2.gameServerUrl, playerToken2, matchRet2.roomId, 'zum2');
        let gameClient3 = await joinRoomUseGameServer(matchRet3.gameServerUrl, playerToken3, matchRet3.roomId, 'zum3');

        await gameClient3.sendRoomMsg({
            recvType: ERoomMsgRecvType.ROOM_OTHERS,
            msg: '大伙好呀~'
        });

        await gameClient1.disconnect();
        await gameClient2.disconnect();
        await gameClient3.disconnect();

    }

    //取消匹配
    {
        //==========这里模拟服务端获取playerToken (openid需要按单元测试名区分一下,防止多个单元测试并行时token互踢)
        const auth1 = await authPlayerToken("zum0001_ApiPlayerMatchRoomJoinUs", "zum1");
        const auth2 = await authPlayerToken("zum0002_ApiPlayerMatchRoomJoinUs", "zum2");
        const auth3 = await authPlayerToken("zum0003_ApiPlayerMatchRoomJoinUs", "zum3");
        //==========end
        let playerToken1 = auth1.playerToken;
        let playerId1 = auth1.playerId;
        let playerToken2 = auth2.playerToken;
        let playerId2 = auth2.playerId;
        let playerToken3 = auth3.playerToken;
        let playerId3 = auth3.playerId;

        let matchReqId1 = await requestMatchOneSingle(playerToken1, playerId1, 8, 3);
        let matchReqId2 = await requestMatchOneSingle(playerToken2, playerId2, 8, 3);
        //取消玩家1匹配
        await cancelMatch(playerToken1, matchReqId1);
        let matchReqId3 = await requestMatchOneSingle(playerToken3, playerId3, 8, 3);

        //延时
        await delay(500);

        //这个时候不满3个,有2个在匹配池等待中,所以没有结果
        let retM1 = await hallClient.queryMatch(playerToken1, matchReqId1);
        let retM2 = await hallClient.queryMatch(playerToken2, matchReqId2);
        let retM3 = await hallClient.queryMatch(playerToken3, matchReqId3);
        assert.ok(retM1 === null, '应该查不到结果的!' + JSON.stringify(retM1));
        assert.ok(retM2 === null, '应该查不到结果的!' + JSON.stringify(retM2));
        assert.ok(retM3 === null, '应该查不到结果的!' + JSON.stringify(retM3));

        //玩家1再提交匹配
        matchReqId1 = await requestMatchOneSingle(playerToken1, playerId1, 8, 3);

        //延时
        await delay(500);

        //这个时候应该有结果了
        retM1 = await hallClient.queryMatch(playerToken1, matchReqId1);
        retM2 = await hallClient.queryMatch(playerToken2, matchReqId2);
        retM3 = await hallClient.queryMatch(playerToken3, matchReqId3);
        assert.ok(retM1 !== null, '应该要有结果了!');
        assert.ok(retM1?.succ === true, '应该匹配成功才对!' + retM1?.err);
        assert.ok(retM2 !== null, '应该要有结果了!');
        assert.ok(retM2?.succ === true, '应该匹配成功才对!' + retM2?.err);
        assert.ok(retM3 !== null, '应该要有结果了!');
        assert.ok(retM3?.succ === true, '应该匹配成功才对!' + retM3?.err);

        let matchRet1 = retM1!.data!;
        let matchRet2 = retM2!.data!;
        let matchRet3 = retM3!.data!;
        let gameClient1 = await joinRoomUseGameServer(matchRet1.gameServerUrl, playerToken1, matchRet1.roomId, 'zum1');
        let gameClient2 = await joinRoomUseGameServer(matchRet2.gameServerUrl, playerToken2, matchRet2.roomId, 'zum2');
        let gameClient3 = await joinRoomUseGameServer(matchRet3.gameServerUrl, playerToken3, matchRet3.roomId, 'zum3');

        //房间要进一下都退出,让房间自然解散,防止有一个招人匹配挂着
        await gameClient1.disconnect();
        await gameClient2.disconnect();
        await gameClient3.disconnect();
    }


    //发起全房间玩家匹配_单人混战
    {
        //==========这里模拟服务端获取playerToken (openid需要按单元测试名区分一下,防止多个单元测试并行时token互踢)
        let auth1 = await authPlayerToken("zum0001_RoomAllPlayersMatchSingle", "zum1");
        let auth2 = await authPlayerToken("zum0002_RoomAllPlayersMatchSingle", "zum2");
        let auth3 = await authPlayerToken("zum0003_RoomAllPlayersMatchSingle", "zum3");
        let auth4 = await authPlayerToken("zum0004_RoomAllPlayersMatchSingle", "zum4");
        //==========end
        let playerToken1 = auth1.playerToken;
        let playerId1 = auth1.playerId;
        let playerToken2 = auth2.playerToken;
        let playerId2 = auth2.playerId;
        let playerToken3 = auth3.playerToken;
        let playerId3 = auth3.playerId;
        let playerToken4 = auth4.playerToken;
        let playerId4 = auth4.playerId;
    
        //创建房间
        let gameClient1Ret = await createAndEnterRoom(playerToken1, playerId1, 'zum1', 3, false);
        let gameClient1 = gameClient1Ret.gameClient;
        let roomId1 = gameClient1Ret.roomId;
        let gameClient2 = await joinRoom(playerToken2, roomId1, 'zum2');
    
        let gameClient3Ret = await createAndEnterRoom(playerToken3, playerId3, 'zum3', 3, false);
        let gameClient3 = gameClient3Ret.gameClient;
        let roomId2 = gameClient3Ret.roomId;
        let gameClient4 = await joinRoom(playerToken4, roomId2, 'zum4');
    
        //发起房间所有玩家匹配请求
        let msgCount = 0;
        gameClient1.onRoomAllPlayersMatchStart = (matchReqId, reqPlayerId, matchParams) => {
            assert.ok(reqPlayerId === playerId1,
                `【玩家1】应该收到玩家1的通知, 实际为${reqPlayerId}`);
            msgCount++;
        };
        gameClient2.onRoomAllPlayersMatchStart = (matchReqId, reqPlayerId, matchParams) => {
            assert.ok(reqPlayerId === playerId1,
                `【玩家2】应该收到玩家1的通知, 实际为${reqPlayerId}`);
            msgCount++;
        };
        let reqRet = await gameClient1.requestMatch({
            matchFromType: EMatchFromType.RoomAllPlayers,
            matchFromInfo: {},
            maxPlayers: 4,
            matcherKey: MatcherKeys.Single,
            matcherParams: {
                minPlayers: 4,
            } as ISingleMatcherParams,
        });
        assert.ok(reqRet.succ, `${reqRet.err}`);
        await delay(200);
        assert.ok(msgCount === 2, `应该要收到2个消息！实际${msgCount}个`);
    
        //趁着还没结果先取消测试一下
        msgCount = 0;
        gameClient1.onRoomAllPlayersMatchResult = (errMsg, errCode, matchResult) => {
            assert.ok(errCode === ErrorCodes.MatchRequestCancelled,
                `【玩家2】应该收到取消结果, 实际为${errCode},${errMsg}`);
            msgCount++;
        };
        gameClient2.onRoomAllPlayersMatchResult = (errMsg, errCode, matchResult) => {
            assert.ok(errCode === ErrorCodes.MatchRequestCancelled,
                `【玩家2】应该收到取消结果, 实际为${errCode},${errMsg}`);
            msgCount++;
        };
        //玩家2去取消
        let cancelRet = await gameClient2.cancelMatch();
        assert.ok(cancelRet.succ, `${cancelRet.err}`);
        await delay(200);
        assert.ok(msgCount === 2, `应该要收到2个消息！实际${msgCount}个`);
    
        //测试正常的匹配成功
        let p1Result!: IMatchPlayerResultWithServer, 
            p2Result!: IMatchPlayerResultWithServer, 
            p3Result!: IMatchPlayerResultWithServer, 
            p4Result!: IMatchPlayerResultWithServer;
        msgCount = 0;
        gameClient1.onRoomAllPlayersMatchStart = (matchReqId, reqPlayerId, matchParams) => {
            msgCount++;
        };
        gameClient1.onRoomAllPlayersMatchResult = (errMsg, errCode, matchResult) => {
            assert.ok(matchResult, `应该有结果,但为空,错误消息为:${errCode},${errMsg}`);
            msgCount++;
            p1Result = matchResult!;
        };
        gameClient2.onRoomAllPlayersMatchStart = (matchReqId, reqPlayerId, matchParams) => {
            msgCount++;
        };
        gameClient2.onRoomAllPlayersMatchResult = (errMsg, errCode, matchResult) => {
            assert.ok(matchResult, `应该有结果,但为空,错误消息为:${errCode},${errMsg}`);
            msgCount++;
            p2Result = matchResult!;
        };
        reqRet = await gameClient1.requestMatch({
            matchFromType: EMatchFromType.RoomAllPlayers,
            matchFromInfo: {},
            maxPlayers: 4,
            matcherKey: MatcherKeys.Single,
            matcherParams: {
                minPlayers: 4,
            } as ISingleMatcherParams,
        });
        assert.ok(reqRet.succ, `${reqRet.err}`);
    
        gameClient3.onRoomAllPlayersMatchStart = (matchReqId, reqPlayerId, matchParams) => {
            msgCount++;
        };
        gameClient3.onRoomAllPlayersMatchResult = (errMsg, errCode, matchResult) => {
            assert.ok(matchResult, `应该有结果,但为空,错误消息为:${errCode},${errMsg}`);
            msgCount++;
            p3Result = matchResult!;
        };
        gameClient4.onRoomAllPlayersMatchStart = (matchReqId, reqPlayerId, matchParams) => {
            msgCount++;
        };
        gameClient4.onRoomAllPlayersMatchResult = (errMsg, errCode, matchResult) => {
            assert.ok(matchResult, `应该有结果,但为空,错误消息为:${errCode},${errMsg}`);
            msgCount++;
            p4Result = matchResult!;
        };
        reqRet = await gameClient3.requestMatch({
            matchFromType: EMatchFromType.RoomAllPlayers,
            matchFromInfo: {},
            maxPlayers: 4,
            matcherKey: MatcherKeys.Single,
            matcherParams: {
                minPlayers: 4,
            } as ISingleMatcherParams,
        });
        assert.ok(reqRet.succ, `${reqRet.err}`);
        await delay(2000);//匹配有定时器的
        assert.ok(msgCount === 8, `应该要收到8个消息！实际${msgCount}个`);
    
    
        //接着根据匹配结果加入新的房间
        await gameClient1.disconnect();
        await gameClient2.disconnect();
        await gameClient3.disconnect();
        await gameClient4.disconnect();
        gameClient1 = await joinRoomUseGameServer(p1Result.gameServerUrl, playerToken1, p1Result.roomId, 'zum1');
        gameClient2 = await joinRoomUseGameServer(p2Result.gameServerUrl, playerToken2, p2Result.roomId, 'zum2');
        gameClient3 = await joinRoomUseGameServer(p3Result.gameServerUrl, playerToken3, p3Result.roomId, 'zum3');
        gameClient4 = await joinRoomUseGameServer(p4Result.gameServerUrl, playerToken4, p4Result.roomId, 'zum4');
    
        msgCount = 0;
        gameClient1.onRecvRoomMsg = (msg) => {
            msgCount++;
        };
        gameClient2.onRecvRoomMsg = (msg) => {
            msgCount++;
        };
        gameClient3.onRecvRoomMsg = (msg) => {
            msgCount++;
        };
        gameClient4.onRecvRoomMsg = (msg) => {
            msgCount++;
        };
        let sendRet = await gameClient1.sendRoomMsg({
            recvType: ERoomMsgRecvType.ROOM_ALL,
            msg: '测试是否同一个房间'
        });
        assert.ok(sendRet.succ, `${sendRet.err}`);
        await delay(200);//匹配有定时器的
        assert.ok(msgCount === 4, `应该要收到4个消息！实际${msgCount}个`);
    
        await gameClient1.disconnect();
        await gameClient2.disconnect();
        await gameClient3.disconnect();
        await gameClient4.disconnect();
    }
}, 60 * 1000);
