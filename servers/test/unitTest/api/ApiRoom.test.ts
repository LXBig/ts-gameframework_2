import { assert } from "chai";
import { ErrorCodes } from "../../../src/shared/tsgf/Result";
import { ERoomMsgRecvType } from "../../../src/shared/tsgf/room/IRoomMsg";
import { delay } from "../../../src/shared/tsgf/Utils";
import { authPlayerToken, authToGameServer, authToGameServerByRoomId, createAndEnterRoom, hallClient, joinRoom, joinRoomUseGameServer } from "./ApiUtils";

it('简单创建房间', async function () {

    //==========这里模拟服务端获取playerToken (openid需要按单元测试名区分一下,防止多个单元测试并行时token互踢)
    let auth1 = await authPlayerToken("zum0001_ApiCreateRoom", "zum1");
    //==========end
    let playerToken = auth1.playerToken;
    let playerId = auth1.playerId;

    let gameClient1Ret = await createAndEnterRoom(playerToken, playerId, 'zum1', 4, false);

    await gameClient1Ret.gameClient.disconnect();

});


test('基本创建房间和加入流程', async function () {
    //==========这里模拟服务端获取playerToken (openid需要按单元测试名区分一下,防止多个单元测试并行时token互踢)
    let auth1 = await authPlayerToken("zum0001_ApiCreateRoomAndJoin", "zum1");
    let auth2 = await authPlayerToken("zum0002_ApiCreateRoomAndJoin", "zum2");
    //==========end
    let playerToken1 = auth1.playerToken;
    let playerId1 = auth1.playerId;
    let playerToken2 = auth2.playerToken;
    let playerId2 = auth2.playerId;

    //创建并进入房间
    let gameClient1Ret = await createAndEnterRoom(playerToken1, playerId1, 'zum1', 2, false);
    let gameClient1 = gameClient1Ret.gameClient;

    //玩家2加入玩家1创建好的房间
    let gameClient2 = await joinRoom(playerToken2, gameClient1Ret.roomId, 'zum2');

    await gameClient1.disconnect();
    await gameClient2.disconnect();
});


test('房间加入失败_MaxPlayers1', async function () {
    //==========这里模拟服务端获取playerToken (openid需要按单元测试名区分一下,防止多个单元测试并行时token互踢)
    let auth1 = await authPlayerToken("zum0001_ApiCreateRoomAndJoin_MaxPlayers", "zum1");
    let auth2 = await authPlayerToken("zum0002_ApiCreateRoomAndJoin_MaxPlayers", "zum2");
    //==========end
    let playerToken1 = auth1.playerToken;
    let playerId1 = auth1.playerId;
    let playerToken2 = auth2.playerToken;
    let playerId2 = auth2.playerId;

    //创建单人房间
    let gameClient1Ret = await createAndEnterRoom(playerToken1, playerId1, 'zum1', 1, false);
    let gameClient1 = gameClient1Ret.gameClient;

    //玩家2
    let gameClient2 = await authToGameServerByRoomId(playerToken2, gameClient1Ret.roomId, 'zum2');
    let ret2 = await gameClient2.joinRoom(gameClient1Ret.roomId);
    assert.ok(ret2.succ === false && ret2.code === ErrorCodes.RoomPlayersFull, '加入应该失败的!因为最大人数只有1!');

    await gameClient1.disconnect();
    await gameClient2.disconnect();
});

test('房间加入失败_MaxPlayers2', async function () {
    //==========这里模拟服务端获取playerToken (openid需要按单元测试名区分一下,防止多个单元测试并行时token互踢)
    let auth1 = await authPlayerToken("zum0001_ApiCreateRoomAndJoin_MaxPlayers2", "zum1");
    let auth2 = await authPlayerToken("zum0002_ApiCreateRoomAndJoin_MaxPlayers2", "zum2");
    let auth3 = await authPlayerToken("zum0003_ApiCreateRoomAndJoin_MaxPlayers2", "zum3");
    let auth4 = await authPlayerToken("zum0004_ApiCreateRoomAndJoin_MaxPlayers2", "zum4");
    //==========end
    let playerToken1 = auth1.playerToken;
    let playerId1 = auth1.playerId;
    let playerToken2 = auth2.playerToken;
    let playerId2 = auth2.playerId;
    let playerToken3 = auth3.playerToken;
    let playerId3 = auth3.playerId;
    let playerToken4 = auth4.playerToken;


    //创建单人房间
    let gameClient1Ret = await createAndEnterRoom(playerToken1, playerId1, 'zum1', 2, false);
    let gameClient1 = gameClient1Ret.gameClient;
    let roomId = gameClient1Ret.roomId;
    //玩家2正常加入
    let gameClient2 = await joinRoom(playerToken2, roomId, 'zum2');

    //玩家4先加入到房间所在游戏服务器(先备用)
    let gameClient4 = await authToGameServer('zum4', playerToken4, gameClient1Ret.gameServerUrl);

    //玩家3加入失败
    let gameClient3 = await authToGameServerByRoomId(playerToken3, roomId, 'zum3');
    let ret3 = await gameClient3.joinRoom(roomId);
    assert.ok(ret3.succ === false && ret3.code === ErrorCodes.RoomPlayersFull, '加入应该失败的!因为最大人数只有2!');

    //玩家2主动退出房间,让玩家3再试,应该要能加入
    await gameClient2.leaveRoom();
    ret3 = await gameClient3.joinRoom(roomId);
    assert.ok(ret3.succ, ret3.err);

    //这个时候玩家2再去加入,应该要失败
    let ret2 = await gameClient2.joinRoom(roomId);
    assert.ok(ret2.succ === false && ret2.code === ErrorCodes.RoomPlayersFull, '加入应该失败的!因为最大人数只有2!');

    //玩家3直接断开,玩家2应该要能加入成功
    await gameClient3.disconnect();
    ret2 = await gameClient2.joinRoom(roomId);
    assert.ok(ret2.succ, ret2.err);

    //玩家1和玩家2都断开,这个时候房间应该被解散了
    await gameClient1.disconnect();
    await gameClient2.disconnect();

    //重新连应该是失败的,先用大厅获取房间注册信息,应该也是要销毁的
    let regRet = await hallClient.getRoomRegInfo(playerToken1, roomId);
    assert.ok(regRet.succ === false, '大厅获取房间注册信息应该是要返回不存在!');
    assert.ok(regRet.code === ErrorCodes.RoomNotFound, '大厅获取房间注册信息应该是要返回不存在!但返回的错误码是' + regRet.code);
    //然后用玩家4(前面已经连接到同一台游戏服务器的),尝试加入房间,应该也是要失败的
    let ret4 = await gameClient4.joinRoom(roomId);
    assert.ok(ret4.succ === false && ret4.code === ErrorCodes.RoomNotFound, '游戏服务器的加入房间应该是要返回不存在!');

});


test('玩家队伍操作_Fixed', async function () {
    //==========这里模拟服务端获取playerToken (openid需要按单元测试名区分一下,防止多个单元测试并行时token互踢)
    let auth1 = await authPlayerToken("zum0001_ApiPlayerTeamFixed", "zum1");
    let auth2 = await authPlayerToken("zum0002_ApiPlayerTeamFixed", "zum2");
    let auth3 = await authPlayerToken("zum0003_ApiPlayerTeamFixed", "zum3");
    //==========end
    let playerToken1 = auth1.playerToken;
    let playerId1 = auth1.playerId;
    let playerToken2 = auth2.playerToken;
    let playerId2 = auth2.playerId;
    let playerToken3 = auth3.playerToken;
    let playerId3 = auth3.playerId;

    //创建房间, 2个队伍,队伍最大人数2
    let gameClient1Ret = await createAndEnterRoom(playerToken1, playerId1, 'zum1', 3, false, '1', 2, 1, 2);
    let gameClient1 = gameClient1Ret.gameClient;
    let roomId = gameClient1Ret.roomId;

    let gameClient2 = await joinRoom(playerToken2, roomId, 'zum2', '1');
    let gameClient3 = await authToGameServerByRoomId(playerToken3, roomId, 'zum3');

    let joinRet = await gameClient3.joinRoom(roomId, '3');
    assert.ok(joinRet.code === ErrorCodes.RoomTeamNotFound,
        `固定房间的队伍不存在,应该不能加入才对!${joinRet.succ},${joinRet.code},${joinRet.err}`);

    joinRet = await gameClient3.joinRoom(roomId, '1');
    assert.ok(joinRet.code === ErrorCodes.RoomTeamPlayersFull,
        `队伍满员了,应该不能加入才对!${joinRet.succ},${joinRet.code},${joinRet.err}`);

    joinRet = await gameClient3.joinRoom(roomId, '2');
    assert.ok(joinRet.succ, joinRet.err);

    let changeTeamRet = await gameClient3.changePlayerTeam('1');
    assert.ok(changeTeamRet.code === ErrorCodes.RoomTeamPlayersFull,
        `队伍满员了,应该不能加入才对!${changeTeamRet.succ},${changeTeamRet.code},${changeTeamRet.err}`);

    let msgCount = 0;
    gameClient1.onChangePlayerTeam = (changeInfo) => {
        assert.ok(changeInfo.changePlayerId === playerId1,
            `【玩家1】应该收到玩家1的通知, 实际为${changeInfo.changePlayerId}`);
        msgCount++;
    };
    gameClient2.onChangePlayerTeam = (changeInfo) => {
        assert.ok(changeInfo.changePlayerId === playerId1,
            `【玩家2】应该收到玩家1的通知, 实际为${changeInfo.changePlayerId}`);
        msgCount++;
    };
    gameClient3.onChangePlayerTeam = (changeInfo) => {
        assert.ok(changeInfo.changePlayerId === playerId1,
            `【玩家3】应该收到玩家1的通知, 实际为${changeInfo.changePlayerId}`);
        msgCount++;
    };
    changeTeamRet = await gameClient1.changePlayerTeam('2');
    assert.ok(changeTeamRet.succ, `${changeTeamRet.err}`);
    await delay(200);//延时一下，因为通知消息是异步的，不会等待本rpc回来再通知
    assert.ok(msgCount === 3, `应该要收到3个消息！实际${msgCount}个`);

    changeTeamRet = await gameClient2.changePlayerTeam('2');
    assert.ok(changeTeamRet.code === ErrorCodes.RoomTeamPlayersFull,
        `队伍满员了,应该不能加入才对!${changeTeamRet.succ},${changeTeamRet.code},${changeTeamRet.err}`);

    msgCount = 0;
    gameClient1.onChangePlayerTeam = (changeInfo) => {
        assert.ok(changeInfo.changePlayerId === playerId3,
            `【玩家1】应该收到通知, 实际为${changeInfo.changePlayerId}`);
        msgCount++;
    };
    gameClient2.onChangePlayerTeam = (changeInfo) => {
        assert.ok(changeInfo.changePlayerId === playerId3,
            `【玩家2】应该收到通知, 实际为${changeInfo.changePlayerId}`);
        msgCount++;
    };
    gameClient3.onChangePlayerTeam = (changeInfo) => {
        assert.ok(changeInfo.changePlayerId === playerId3,
            `【玩家3】应该收到通知, 实际为${changeInfo.changePlayerId}`);
        msgCount++;
    };
    changeTeamRet = await gameClient3.changePlayerTeam('1');
    assert.ok(changeTeamRet.succ, `${changeTeamRet.err}`);
    await delay(200);//延时一下，因为通知消息是异步的，不会等待本rpc回来再通知
    assert.ok(msgCount === 3, `应该要收到3个消息！实际${msgCount}个`);


    await gameClient1.disconnect();
    await gameClient2.disconnect();
    await gameClient3.disconnect();
});


test('玩家队伍操作_Free', async function () {
    //==========这里模拟服务端获取playerToken (openid需要按单元测试名区分一下,防止多个单元测试并行时token互踢)
    let auth1 = await authPlayerToken("zum0001_ApiPlayerTeamFree", "zum1");
    let auth2 = await authPlayerToken("zum0002_ApiPlayerTeamFree", "zum2");
    let auth3 = await authPlayerToken("zum0003_ApiPlayerTeamFree", "zum3");
    //==========end
    let playerToken1 = auth1.playerToken;
    let playerId1 = auth1.playerId;
    let playerToken2 = auth2.playerToken;
    let playerId2 = auth2.playerId;
    let playerToken3 = auth3.playerToken;
    let playerId3 = auth3.playerId;

    //创建房间, 2个队伍,队伍最大人数2
    let gameClient1Ret = await createAndEnterRoom(playerToken1, playerId1, 'zum1', 3, false, '1',
        undefined, undefined, undefined,
        1, 2);
    let gameClient1 = gameClient1Ret.gameClient;
    let roomId = gameClient1Ret.roomId;

    let gameClient2 = await joinRoom(playerToken2, roomId, 'zum2', '1');
    let gameClient3 = await authToGameServerByRoomId(playerToken3, roomId, 'zum3');

    let joinRet = await gameClient3.joinRoom(roomId, '1');
    assert.ok(joinRet.code === ErrorCodes.RoomTeamPlayersFull,
        `队伍满员了,应该不能加入才对!${joinRet.succ},${joinRet.code},${joinRet.err}`);

    joinRet = await gameClient3.joinRoom(roomId, '2');
    assert.ok(joinRet.succ, joinRet.err);

    let changeTeamRet = await gameClient3.changePlayerTeam('1');
    assert.ok(changeTeamRet.code === ErrorCodes.RoomTeamPlayersFull,
        `队伍满员了,应该不能加入才对!${changeTeamRet.succ},${changeTeamRet.code},${changeTeamRet.err}`);

    let msgCount = 0;
    gameClient1.onChangePlayerTeam = (changeInfo) => {
        assert.ok(changeInfo.changePlayerId === playerId1,
            `【玩家1】应该收到玩家1的通知, 实际为${changeInfo.changePlayerId}`);
        msgCount++;
    };
    gameClient2.onChangePlayerTeam = (changeInfo) => {
        assert.ok(changeInfo.changePlayerId === playerId1,
            `【玩家2】应该收到玩家1的通知, 实际为${changeInfo.changePlayerId}`);
        msgCount++;
    };
    gameClient3.onChangePlayerTeam = (changeInfo) => {
        assert.ok(changeInfo.changePlayerId === playerId1,
            `【玩家3】应该收到玩家1的通知, 实际为${changeInfo.changePlayerId}`);
        msgCount++;
    };
    changeTeamRet = await gameClient1.changePlayerTeam('2');
    assert.ok(changeTeamRet.succ, `${changeTeamRet.err}`);
    await delay(200);//延时一下，因为通知消息是异步的，不会等待本rpc回来再通知
    assert.ok(msgCount === 3, `应该要收到3个消息！实际${msgCount}个`);

    changeTeamRet = await gameClient2.changePlayerTeam('2');
    assert.ok(changeTeamRet.code === ErrorCodes.RoomTeamPlayersFull,
        `队伍满员了,应该不能加入才对!${changeTeamRet.succ},${changeTeamRet.code},${changeTeamRet.err}`);

    //玩家2也改到其他队伍.队伍1应该已经不存在了
    changeTeamRet = await gameClient2.changePlayerTeam('3');
    assert.ok(changeTeamRet.succ, `${changeTeamRet.err}`);
    assert.ok(gameClient2.currRoomInfo && !gameClient2.currRoomInfo.teamList.find(t => t.id === '1'), `队伍1应该已经不存在了!`);

    msgCount = 0;
    gameClient1.onChangePlayerTeam = (changeInfo) => {
        assert.ok(changeInfo.changePlayerId === playerId3,
            `【玩家1】应该收到通知, 实际为${changeInfo.changePlayerId}`);
        msgCount++;
    };
    gameClient2.onChangePlayerTeam = (changeInfo) => {
        assert.ok(changeInfo.changePlayerId === playerId3,
            `【玩家2】应该收到通知, 实际为${changeInfo.changePlayerId}`);
        msgCount++;
    };
    gameClient3.onChangePlayerTeam = (changeInfo) => {
        assert.ok(changeInfo.changePlayerId === playerId3,
            `【玩家3】应该收到通知, 实际为${changeInfo.changePlayerId}`);
        msgCount++;
    };
    changeTeamRet = await gameClient3.changePlayerTeam('1');
    assert.ok(changeTeamRet.succ, `${changeTeamRet.err}`);
    await delay(200);//延时一下，因为通知消息是异步的，不会等待本rpc回来再通知
    assert.ok(msgCount === 3, `应该要收到3个消息！实际${msgCount}个`);


    changeTeamRet = await gameClient3.changePlayerTeam('3');
    assert.ok(changeTeamRet.succ, `${changeTeamRet.err}`);


    await gameClient1.disconnect();
    await gameClient2.disconnect();
    await gameClient3.disconnect();
});


test('创建房间和多玩家加入房间以及事件触发逻辑', async function () {

    //==========这里模拟服务端获取playerToken (openid需要按单元测试名区分一下,防止多个单元测试并行时token互踢)
    const auth1 = await authPlayerToken("zum0001_ApiRoomJoinAndMsg", "zum1");
    const auth2 = await authPlayerToken("zum0002_ApiRoomJoinAndMsg", "zum2");
    const auth3 = await authPlayerToken("zum0003_ApiRoomJoinAndMsg", "zum3");
    //==========end
    let playerToken = auth1.playerToken;
    let playerId = auth1.playerId;
    let playerToken2 = auth2.playerToken;
    let playerToken3 = auth3.playerToken;

    //玩家1创建房间并进入游戏服务器
    let gameClient1Ret = await createAndEnterRoom(playerToken, playerId, 'zum1');
    let gameClient1 = gameClient1Ret.gameClient;
    let roomId = gameClient1Ret.roomId;

    //玩家2加入玩家1创建的房间（进入游戏服务器），玩家1收到来人通知
    let msgCount = 0;
    gameClient1.onPlayerJoinRoom = (playerInfo, roomInfo) => {
        assert.ok(playerInfo.playerId === auth2.playerId, '这个时候【玩家1】应该收到【玩家2】的进入房间通知');
        msgCount++;
    };
    let gameClient2 = await joinRoom(playerToken2, roomId, 'zum2');
    await delay(200);//延时一下，因为通知消息是异步的，不会等待本rpc回来再通知
    assert.ok(msgCount === 1, '应该要收到1个消息！');

    msgCount = 0;
    gameClient1.onPlayerJoinRoom = (playerInfo, roomInfo) => {
        assert.ok(playerInfo.playerId === auth3.playerId, '这个时候【玩家1】应该收到【玩家3】的进入房间通知');
        msgCount++;
    };
    gameClient2.onPlayerJoinRoom = (playerInfo, roomInfo) => {
        assert.ok(playerInfo.playerId === auth3.playerId, '这个时候【玩家2】应该收到【玩家3】的进入房间通知');
        msgCount++;
    };
    //玩家3加入玩家1创建的房间（进入游戏服务器），玩家2、玩家2收到来人通知
    let gameClient3 = await joinRoom(playerToken3, roomId, 'zum3');
    await delay(200);//延时一下，因为通知消息是异步的，不会等待本rpc回来再通知
    assert.ok(msgCount === 2, '应该要收到2个消息！');

    //玩家2发送房间消息
    msgCount = 0;
    gameClient1.onRecvRoomMsg = (msg) => {
        assert.ok(msg.fromPlayerInfo.playerId === auth2.playerId, '这个时候【玩家1】应该收到【玩家2】的房间广播消息');
        msgCount++;
    };
    gameClient2.onRecvRoomMsg = (msg) => {
        assert.ok(msg.fromPlayerInfo.playerId === auth2.playerId, '这个时候【玩家2】应该收到【玩家2】的房间广播消息');
        msgCount++;
    };
    gameClient3.onRecvRoomMsg = (msg) => {
        assert.ok(msg.fromPlayerInfo.playerId === auth2.playerId, '这个时候【玩家3】应该收到【玩家2】的房间广播消息');
        msgCount++;
    };
    await gameClient2.sendRoomMsg({
        recvType: ERoomMsgRecvType.ROOM_ALL,
        msg: '广播所有人一条测试消息'
    });
    await delay(200);//延时一下，因为通知消息是异步的，不会等待本rpc回来再通知
    assert.ok(msgCount === 3, '应该要收到3个消息！');


    msgCount = 0;
    gameClient1.onRecvRoomMsg = (msg) => {
        assert.ok(msg.fromPlayerInfo.playerId === auth2.playerId, '这个时候【玩家1】应该收到【玩家2】的房间给他人的消息');
        msgCount++;
    };
    gameClient2.onRecvRoomMsg = (msg) => {
        assert.fail('这个时候【玩家2】不应该收到【玩家2】的房间消息');
    };
    gameClient3.onRecvRoomMsg = (msg) => {
        assert.ok(msg.fromPlayerInfo.playerId === auth2.playerId, '这个时候【玩家3】应该收到【玩家2】的房间给他人的消息');
        msgCount++;
    };
    await gameClient2.sendRoomMsg({
        recvType: ERoomMsgRecvType.ROOM_OTHERS,
        msg: '其他人，我是玩家1'
    });
    await delay(200);//延时一下，因为通知消息是异步的，不会等待本rpc回来再通知
    assert.ok(msgCount === 2, '应该要收到2个消息！');


    msgCount = 0;
    gameClient1.onRecvRoomMsg = (msg) => {
        assert.ok(msg.fromPlayerInfo.playerId === auth2.playerId, '这个时候【玩家1】应该收到【玩家2】的房间指定消息');
        msgCount++;
    };
    gameClient2.onRecvRoomMsg = (msg) => {
        assert.fail('这个时候【玩家2】不应该收到【玩家2】的房间消息');
    };
    gameClient3.onRecvRoomMsg = (msg) => {
        assert.fail('这个时候【玩家3】不应该收到【玩家2】的房间消息');
    };
    await gameClient2.sendRoomMsg({
        recvType: ERoomMsgRecvType.ROOM_SOME,
        recvPlayerList: [gameClient2.currRoomInfo!.ownerPlayerId],
        msg: '房主好，我是玩家1',
    });
    await delay(200);//延时一下，因为通知消息是异步的，不会等待本rpc回来再通知
    assert.ok(msgCount === 1, '应该要收到1个消息！');


    //测试房间信息修改
    msgCount = 0;
    gameClient1.onChangeRoom = (roomInfo) => {
        assert.ok(roomInfo.roomName === '2', `【玩家1】roomName应为'2', 实际为${roomInfo.roomName}`);
        msgCount++;
    };
    gameClient2.onChangeRoom = (roomInfo) => {
        assert.ok(roomInfo.roomName === '2', `【玩家2】roomName应为'2', 实际为${roomInfo.roomName}`);
        msgCount++;
    };
    gameClient3.onChangeRoom = (roomInfo) => {
        assert.ok(roomInfo.roomName === '2', `【玩家3】roomName应为'2', 实际为${roomInfo.roomName}`);
        msgCount++;
    };
    let changeRoomRet = await gameClient1.changeRoom({
        roomName: '2',
    });
    assert.ok(changeRoomRet.succ, `修改房间信息应该成功.${changeRoomRet.err}`)
    await delay(200);//延时一下，因为通知消息是异步的，不会等待本rpc回来再通知
    assert.ok(msgCount === 3, `应该要收到3个消息！实际${msgCount}个`);


    //测试玩家自定义属性修改
    msgCount = 0;
    gameClient1.onChangeCustomPlayerProfile = (changeInfo) => {
        assert.ok(changeInfo.changePlayerId === playerId,
            `【玩家1】应该收到玩家1的通知, 实际为${changeInfo.changePlayerId}`);
        msgCount++;
    };
    gameClient2.onChangeCustomPlayerProfile = (changeInfo) => {
        assert.ok(changeInfo.changePlayerId === playerId,
            `【玩家2】应该收到玩家1的通知, 实际为${changeInfo.changePlayerId}`);
        msgCount++;
    };
    gameClient3.onChangeCustomPlayerProfile = (changeInfo) => {
        assert.ok(changeInfo.changePlayerId === playerId,
            `【玩家3】应该收到玩家1的通知, 实际为${changeInfo.changePlayerId}`);
        msgCount++;
    };
    let changeProfileRet = await gameClient1.changeCustomPlayerProfile('2');
    assert.ok(changeProfileRet.succ, `修改玩家自定义属性应该成功.${changeProfileRet.err}`)
    await delay(200);//延时一下，因为通知消息是异步的，不会等待本rpc回来再通知
    assert.ok(msgCount === 3, `应该要收到3个消息！实际${msgCount}个`);

    //测试玩家自定义状态修改
    msgCount = 0;
    gameClient1.onChangeCustomPlayerStatus = (changeInfo) => {
        assert.ok(changeInfo.changePlayerId === playerId,
            `【玩家1】应该收到玩家1的通知, 实际为${changeInfo.changePlayerId}`);
        msgCount++;
    };
    gameClient2.onChangeCustomPlayerStatus = (changeInfo) => {
        assert.ok(changeInfo.changePlayerId === playerId,
            `【玩家2】应该收到玩家1的通知, 实际为${changeInfo.changePlayerId}`);
        msgCount++;
    };
    gameClient3.onChangeCustomPlayerStatus = (changeInfo) => {
        assert.ok(changeInfo.changePlayerId === playerId,
            `【玩家3】应该收到玩家1的通知, 实际为${changeInfo.changePlayerId}`);
        msgCount++;
    };
    let changeStatusRet = await gameClient1.changeCustomPlayerStatus(2);
    assert.ok(changeStatusRet.succ, `修改玩家自定义状态应该成功.${changeStatusRet.err}`)
    await delay(200);//延时一下，因为通知消息是异步的，不会等待本rpc回来再通知
    assert.ok(msgCount === 3, `应该要收到3个消息！实际${msgCount}个`);


    //玩家2离开房间, 玩家1、玩家2收到通知
    msgCount = 0;
    gameClient1.onPlayerLeaveRoom = (playerInfo, roomInfo) => {
        assert.ok(playerInfo.playerId === auth2.playerId, '这个时候【玩家1】应该收到【玩家2】的退出房间通知');
        msgCount++;
    };
    gameClient2.onPlayerLeaveRoom = (playerInfo, roomInfo) => {
        assert.fail('这个时候【玩家2】不应该收到【玩家2】的退出房间通知');
    };
    gameClient3.onPlayerLeaveRoom = (playerInfo, roomInfo) => {
        assert.ok(playerInfo.playerId === auth2.playerId, '这个时候【玩家3】应该收到【玩家2】的退出房间通知');
        msgCount++;
    };
    await gameClient2.leaveRoom();
    await delay(200);//延时一下，因为通知消息是异步的，不会等待本rpc回来再通知
    assert.ok(msgCount === 2, `应该要收到2个消息！实际${msgCount}个`);

    //玩家2离开房间,测试一下不在房间中是否不应该收到通知
    msgCount = 0;
    gameClient1.onChangeCustomPlayerStatus = (changeInfo) => {
        msgCount++;
        assert.fail(`【玩家1】不应该收到通知, 实际changePlayerId为${changeInfo.changePlayerId}`);
    };
    gameClient2.onChangeCustomPlayerStatus = (changeInfo) => {
        msgCount++;
        assert.fail(`【玩家2】不应该收到通知, 实际changePlayerId为${changeInfo.changePlayerId}`);
    };
    gameClient3.onChangeCustomPlayerStatus = (changeInfo) => {
        msgCount++;
        assert.fail(`【玩家3】不应该收到通知, 实际changePlayerId为${changeInfo.changePlayerId}`);
    };
    changeStatusRet = await gameClient2.changeCustomPlayerStatus(2);
    assert.ok(changeStatusRet.succ, `修改玩家自定义状态应该成功.${changeStatusRet.err}`)
    await delay(200);//延时一下，因为通知消息是异步的，不会等待本rpc回来再通知
    assert.ok(msgCount === 0, `应该要收到0个消息！实际${msgCount}个`);

    //玩家1解散房间，玩家3收到解散通知
    msgCount = 0;
    gameClient1.onDismissRoom = (roomInfo) => {
        assert.fail('这个时候【玩家1】不应该收到解散房间通知');
    };
    gameClient2.onDismissRoom = (roomInfo) => {
        assert.fail('这个时候【玩家2】不应该收到解散房间通知');
    };
    gameClient3.onDismissRoom = (roomInfo) => {
        msgCount++;
    };
    await gameClient1.dismissRoom();
    await delay(200);//延时一下，因为通知消息是异步的，不会等待本rpc回来再通知
    assert.ok(msgCount === 1, '应该要收到1个消息！');

    //await delay(5000);//临时延迟5秒，方便我查看一下redis


    await gameClient1.disconnect();
    await gameClient2.disconnect();
    await gameClient3.disconnect();
}, 60 * 1000);

