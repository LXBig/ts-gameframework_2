import { assert } from "chai";
import { authPlayerToken } from "./ApiUtils";

it('服务端认证(playerToken)', async function () {
    //==========这里模拟服务端获取playerToken (openid需要按单元测试名区分一下,防止多个单元测试并行时token互踢)
    await authPlayerToken("zum0001_ApiAuthorize", "zum1");
})